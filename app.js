/* Load the AWS SDK for Node.js */
const AWS        = require('aws-sdk');
const request    = require('request');
const UUID       = require('/opt/bitnami/apache2/htdocs/bee-client-api/uuid.js');
const crypto     = require('crypto');
const placements = require('/opt/bitnami/apache2/htdocs/bee-client-api/config.json');

AWS.config.update({
    accessKeyId: 	 'AKIAJ2HE4GU3YAO472CA',
    secretAccessKey: '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',
    region: 		 'eu-central-1'
});

const s3         = new AWS.S3();
const dynamodb   = new AWS.DynamoDB();
const lambda     = new AWS.Lambda();
const docClient  = new AWS.DynamoDB.DocumentClient();

const http 	  = require('http');
const https   = require('https');

const cron    = require("node-cron");
const express = require("express");
const cluster = require('cluster');
const fs      = require("fs");
const os      = require('os');

// ip2location Module
var ip2loc = require("ip2location-nodejs");
ip2loc.IP2Location_init("/opt/bitnami/apache2/htdocs/bee-client-api/ip2location-db/IP-COUNTRY-REGION-CITY-LATITUDE-LONGITUDE-ZIPCODE-ISP-DOMAIN.BIN");

// Certificate
const privateKey 	= fs.readFileSync('/etc/letsencrypt/live/bee-client-api.castoola.tv/privkey.pem', 	'utf8');
const certificate 	= fs.readFileSync('/etc/letsencrypt/live/bee-client-api.castoola.tv/cert.pem', 		'utf8');
const ca 			= fs.readFileSync('/etc/letsencrypt/live/bee-client-api.castoola.tv/chain.pem', 	'utf8');

const credentials = {
	key:  privateKey,
	cert: certificate,
	ca:   ca
};

app = express();

var ddb = new AWS.DynamoDB({
    apiVersion: '2012-08-10'
});

var imageHex = "\x42\x4d\x3c\x00\x00\x00\x00\x00\x00\x00\x36\x00\x00\x00\x28\x00"+ 
"\x00\x00\x01\x00\x00\x00\x01\x00\x00\x00\x01\x00\x18\x00\x00\x00"+ 
"\x00\x00\x06\x00\x00\x00\x27\x00\x00\x00\x27\x00\x00\x00\x00\x00"+
"\x00\x00\x00\x00\x00\x00\xff\xff\xff\xff\x00\x00"; 

// Code to run if we're in the master process
if (cluster.isMaster) {
    var cpuCount = os.cpus().length;

    for (var i = 0; i < cpuCount; i += 1) {
        cluster.fork();
    }

    cluster.on('exit', function (worker) {
        console.log({
        	worker_id: worker.id,
        	event: "exit"
        });

        cluster.fork();
    });
// Code to run if we're in a worker process
} else {
	var cpuCount = os.cpus().length;

    app.get('/', function (req, res) {
		res.sendStatus(200);
        return;
	});
	
	app.get('/v1/dmp/get-client-id', function (req, res) {    	
    	getClientId(req, res);
	});
	
	app.get('/v1/dmp/log-event', function (req, res) {    	
    	logEvent(req, res);
	});
	
	app.get('/v1/collection/post-data', function (req, res) {    	
    	collectionCollect(req, res);
	});
	
	app.get('/v1/ads/get-ad', function (req, res) {    	
    	getAd(req, res);
	});
	
	app.get('/v1/ads/log-impression', function (req, res) {    	
		logImpression(req, res);
	});

	app.get('/v1/ads/log-click', function (req, res) {    	
    	logClick(req, res);
	});

	app.get('/v1/ads/log-conversion', function (req, res) {    	
		logConversion(req, res);
    });

	var httpServer 	= http.createServer(app);
	var httpsServer = https.createServer(credentials, app);

	httpServer.listen(80, function () {
		var host = httpServer.address().address;
	  	var port = httpServer.address().port;
	  	console.log("[bee-client-api] listening at http://%s:%s", host, port);
  	});

	httpsServer.listen(443, function () {
		var host = httpsServer.address().address;
	  	var port = httpsServer.address().port;
	  	console.log("[bee-client-api] listening at https://%s:%s", host, port);
  	});
}

/**
 *	Get client ID
 *		This function is used to register a device. In the case of sucsessful registration,
 *		this function returns Unique User ID (UUID) and client's location (country code, region, city).
 *		UUID is calculated as a hash of Device ID and Profile ID. Device registration must be done prior to making any other API call.
 *	Expected endpoint: 
 *		/v1/dmp/get-client-id
 */
async function getClientId(req, res) {
	/* Default response settings */
    res.setHeader('Content-Type', 'application/json');
    res.status(400);

    /* init */
	var qs, callback, client, clients, audience, audiences;
	var device_id   = "0";
	var profile_id  = "0";
	var output_type = "json";
	var device_id_exists = false;
    var profile_id_exists = false;
    
    /* Required */
    if (checkRequired(req.query.service_id)) {
        res.send(errorObj(1001, "Service ID is invalid or missing."));
		return;
    }
    
    if (checkRequired(req.query.platform)) {
        res.send(errorObj(1002, "Platform is invalid or missing."));
		return;
    }
    
    /* Optional */
	if (checkOptional(req.query.device_id)) {
		device_id_exists = true;
		device_id = req.query.device_id;
	}

	if (checkOptional(req.query.profile_id)) {
		profile_id_exists = true;
		profile_id = req.query.profile_id;
	}

	if (checkOptional(req.query.callback)) {
		output_type = "jsonp";
		callback    = req.query.callback;
    }
    
    var ip          = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
		ip          = ip.replace(/^.*:/, '');
    var ua          = req.headers['user-agent']; 								
    var ip_address  = crypto.createHash('md5').update(ip).digest("hex");
    // var client_uuid = ip.concat(ua);    
	var client_uuid = "";
	
	if (device_id_exists === true || profile_id_exists === true) {
		if (device_id_exists  === true)		client_uuid = client_uuid.concat(device_id);
		if (profile_id_exists === true) 	client_uuid = client_uuid.concat(profile_id);
	} else {
		var ts   	= Date.now().toString();
		var rand 	= getRndInteger(100000000, 999999999);
		client_uuid = ts.concat(rand);
	}

	var client_id = await getUuid(client_uuid);

	/* Get location data */
	var ip2location_data = {}, location = {};
	try {
		// location = await getGeo(ip);    
		ip2location_data = ip2loc.IP2Location_get_all(ip);

		location.geo_cc 	= ip2location_data.country_short;
		location.geo_region = ip2location_data.region;
		location.geo_city 	= ip2location_data.city;
		location.geo_zip 	= ip2location_data.zipcode;
		location.isp 		= ip2location_data.isp;
		location.domain 	= ip2location_data.domain;
	} catch (e) {
		res.send(errorObj(1003, "An unknown error has occurred."));
		return;
    }

    var params = {
	    TableName : "atv_dmp_v1_clients",
	    ExpressionAttributeValues: {
		    ':c_id': {S: client_id}
		},
		KeyConditionExpression: 'client_id = :c_id'
    };
    
	var client_login = await getItem(ddb, params);

	if (client_login.data.length > 0) {
        client = {
            TableName: 'atv_dmp_v1_clients',
            Key:{
                'client_id': {S: client_id}
            },
            UpdateExpression: "set brand = :b, device_id = :did, geo_cc = :gcc, geo_city = :gc, geo_region = :gr, geo_zip = :gz, isp = :isp, #dmn = :dmn, ip_address = :ipa, version = :v", // geo_province = :gp
            ExpressionAttributeValues:{
                ':b'  : {S: getBrand(ua)},
                ':did': {S: device_id},
                ':gcc': {S: location.geo_cc},
                ':gr' : {S: location.geo_region},
                ':gc' : {S: location.geo_city},
				':gz' : {S: location.geo_zip},
				':isp': {S: location.isp},
				':dmn': {S: location.domain},
				// ':gp' : {S: location.geo_province},
                ':ipa': {S: ip_address},
                ':v'  : {S: getVersion(ua)}
			},
			ExpressionAttributeNames:{
				"#dmn": "domain"
			}
        };

        if (profile_id_exists) {
            client.ConditionExpression = 'profile_id = :pid';
            client.ExpressionAttributeValues[':pid'] = {S: profile_id};
        }

		clients = await updateItem(ddb, client);
	} else {
        client = {
            TableName: 'atv_dmp_v1_clients',
            Item: {
                'client_id'   : {S: client_id},
                'brand'       : {S: getBrand(ua)},
                'device_id'   : {S: device_id},
                'geo_cc'      : {S: location.geo_cc},
                'geo_region'  : {S: location.geo_region},
                'geo_city'    : {S: location.geo_city},
				'geo_zip'     : {S: location.geo_zip},
				'isp'     	  : {S: location.isp},
				'domain'      : {S: location.domain},
				// 'geo_province': {S: location.geo_province},
                'ip_address'  : {S: ip_address},
                'version'     : {S: getVersion(ua)}
            }
        };

        if (profile_id_exists) {
            client.Item['profile_id'] = {S: profile_id};
        }

		clients = await putItem(ddb, client);
    }

	/* Audience */
	var ts = Date.now();
	audience = {
		TableName: 'atv_dmp_v1_audience',
		Item: {
			'client_id'   : {S: client_id},
			'ts'          : {S: ts.toString()},
			'brand'       : {S: getBrand(ua)},
			'device_id'   : {S: device_id},
			'geo_cc'      : {S: location.geo_cc},
			'geo_region'  : {S: location.geo_region},
			'geo_city'    : {S: location.geo_city},
			'geo_zip'     : {S: location.geo_zip},
			'isp'     	  : {S: location.isp},
			'domain'      : {S: location.domain},
			// 'geo_province': {S: location.geo_province},
			'ip_address'  : {S: ip_address},
			'platform'    : {S: req.query.platform},
			'service_id'  : {S: req.query.service_id},
			'ua'          : {S: ua},
            'version'     : {S: getVersion(ua)}
		}
    };

    try {
        audiences = await putItem(ddb, audience);
    } catch(e) {
        console.log(e.message);
    }

	if (clients.succeed === true && audiences.succeed === true) {
		var data = {
			client_id:    client_id,
			geo_cc:       location.geo_cc,
			geo_region:   location.geo_region,
			geo_city:     location.geo_city
        };
    
        res.status(200);
		if (output_type === "json") {
            res.send(data);
            return;
		} else {            
			return jsonp(res, callback, data);
		}
	} else {
        res.send(errorObj(1003, "An unknown error has occurred."));
		return;
    }
};

/**
 *	Log event
 *		This function is used to log events. Various types of events can be logged,
 *		with each of the type requesting different additional event data.
 *	Expected endpoint: 
 *		/v1/dmp/log-event
 */
async function logEvent(req, res) {
	/* Default response settings */
	res.setHeader('Content-Type', 'application/json');
	res.status(400);

	/* init */
	var callback, output_type = "px";
	var livetv_id = "", content_id = "";

	/* Required */
    if (checkRequired(req.query.client_id)) {
        res.send(errorObj(2001, "Client ID is invalid or missing."));
		return;
    } else {
		if (!UUID.check(req.query.client_id)) {
			res.send(errorObj(2001, "Client ID is invalid or missing."));
			return;
		}
	}

	if (checkRequired(req.query.publisher_id)) {
        res.send(errorObj(2002, "Publisher ID is invalid or missing."));
		return;
	}

	if (checkRequired(req.query.event_type)) {
		res.send(errorObj(2003, "Event type is invalid or missing."));
		return;
	} else {
		if (req.query.event_type === "livetv") {
			if (checkRequired(req.query.livetv_id)) {
				res.send(errorObj(2004, "Live TV ID is invalid or missing."));
				return;
			} else {
				livetv_id = req.query.livetv_id;
			}
		} else if (req.query.event_type === "content") {
			if (checkRequired(req.query.content_id)) {
				res.send(errorObj(2005, "Content ID is invalid or missing."));
				return;
			} else {
				content_id = req.query.content_id;
			}
		} else {
			res.send(errorObj(2003, "Event type is invalid or missing."));
			return;
		}
	}

	/* Optional */
	if (checkOptional(req.query.output_type)) {
		if (req.query.output_type === "px") {
			output_type = req.query.output_type;
		} else if (req.query.output_type === "json") {
			output_type = req.query.output_type;

			if (checkOptional(req.query.callback)) {
				output_type = "jsonp";
				callback    = req.query.callback;
			}
		} else {
			res.send(errorObj(2006, "Output type is invalid (json or px)."));
			return;
		}
	}
	
	/* Default object */
	var ts = Date.now();
	var params = {
		TableName: 'atv_dmp_v1_watch',
		Item: {
			'client_id'    : {S: req.query.client_id},
			'ts'           : {S: ts.toString()},
			'publisher_id' : {S: req.query.publisher_id},
			'content_name' : {S: req.query.event_type}
		}
	};

	if (livetv_id.length > 0) {
		params.Item.livetv_id  = {S: livetv_id};
	} else if (content_id.length > 0) {
		params.Item.content_id = {S: content_id};
	} else {
		res.send(errorObj(2007, "An unknown error has occurred."));
		return;
	}

	var log_event = await putItem(ddb, params);

	if (log_event.succeed === true) {
		res.status(200);
		if (output_type === "px") {
			return px(res);
		} else if (output_type === "json") {
			res.send({success: "ok"});
			return;
		} else {
			return jsonp(res, callback, 
				{success: "ok"}
			);
		}
	} else {
		res.send(errorObj(2007, "An unknown error has occurred."));
		return;
	}
};

/**
 *	Collect data
 *		This function is used to collect any type of data, such as phone numbers,
 *		or voting choices for example.
 *	Expected endpoint:
 *		/v1/dmp/collection/post-data
 */
async function collectionCollect(req, res) {
	/* Default response settings */
	res.setHeader('Content-Type', 'application/json');
	res.status(400);

	var callback, output_type = "px";

	/* Required */
	if (checkRequired(req.query.client_id)) {
		res.send(errorObj(3001, "Client ID is invalid or missing."));
		return;
	} else {
		if (!UUID.check(req.query.client_id)) {
			res.send(errorObj(3001, "Client ID is invalid or missing."));
			return;
		}
	}

	if (checkRequired(req.query.collection_id)) {
		res.send(errorObj(3002, "Collection ID is invalid or missing."));
		return;
	} else {
		if (!isInteger(req.query.collection_id)) {
			res.send(errorObj(3002, "Collection ID is invalid or missing."));
			return;
		}
	}

	/* Optional */
	if (checkOptional(req.query.output_type)) {
		if (req.query.output_type === "px") {
			output_type = req.query.output_type;
		} else if (req.query.output_type === "json") {
			output_type = req.query.output_type;

			if (checkOptional(req.query.callback)) {
				output_type = "jsonp";
				callback    = req.query.callback;
			}
		} else {
			res.send(errorObj(3003, "Output type is invalid (json or px)."));
			return;
		}
	}
	
	/* Default object */
	var ts = Date.now();
	var params = {
		TableName: 'atv_dmp_v1_collection',
		Item: {
			'client_id'     : {S: req.query.client_id},
			'ts'            : {S: ts.toString()},
			'collection_id' : {S: req.query.collection_id}
		}
	};
	
	var values = Object.entries(req.query).map(
		([key, value]) => ({key,value})
	);

	var v = false, i = 1, val = "val";
	while (true) {
	    var tmp = val.concat(i);
  		var fi = values.find(
    		o => o.key === tmp
  		);

	    if (checkRequired(fi)) {
	    	break;
	    } else {
	    	params.Item[tmp] = {S: fi.value};
	    	v = true;
	    	i++;
	    }
	}
	
	if (v === false) {
		res.send(errorObj(3004, "Data is invalid or missing (at least 1 value needed)."));
		return;
	}

	var collect_data = await putItem(ddb, params);

	if (collect_data.succeed === true) {
		res.status(200);
		if (output_type === "px") {
			return px(res);
		} else if (output_type === "json") {
			res.send({success: "ok"});
			return;
		} else {
			return jsonp(res, callback, 
				{success: "ok"}
			);
		}
	} else {
		res.send(errorObj(3005, "An unknown error has occurred."));
		return;
	}
};

/**
 * 
 * Get ad
 * Expected endpoint:
 *		/v1/ads/get-ad
 */
async function getAd(req, res) {
	/* Default response settings */
    res.setHeader('Content-Type', 'application/json');
	res.status(400);

	var r = {};
	var config_data, x_forwarded_for, host = null, protocol = "http";
	var plc_id, publisher_id, type, callback, client_id, profile_id;
	var geo_cc = "", geo_region = "", geo_city = "";
	var c_ad_type, c_adserver_domain, c_adserver_type, c_adserver_endpoint, c_adserver_zoneid, c_dmp_type, c_dmp_hostname, c_dmp_port;

	x_forwarded_for = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
	x_forwarded_for = x_forwarded_for.replace(/^.*:/, '');
	host 		    = req.hostname;
	protocol		= req.protocol;

	/* 
		Get atributes
	 */
	if (checkRequired(req.query.placement_id)) {
		res.send(errorObj(4001, "Placement ID is invalid or missing."));
		return;
	} else {
		plc_id 		 = req.query.placement_id;
	
		publisher_id = plc_id.split("-");
		publisher_id = publisher_id[0];
	}

	if (checkRequired(req.query.response_type)) {
		res.send(errorObj(4002, "Response type is invalid or missing."));
		return;
	} else {
		if (req.query.response_type === "json" || req.query.response_type === "vast" || req.query.response_type === "script") {
			type = req.query.response_type;
		} else {
			res.send(errorObj(4002, "Response type is invalid or missing."));
			return;
		}
	}

	if (checkRequired(req.query.client_id)) {
		res.send(errorObj(4003, "Client ID is invalid or missing."));
		return;
	} else {
		if (UUID.check(req.query.client_id)) {
			client_id = req.query.client_id;
		} else {
			res.send(errorObj(4003, "Client ID is invalid or missing."));
			return;
		}
	}

	if (checkOptional(req.query.callback)) {
		callback 	= req.query.callback;
	}

	if (checkOptional(req.query.profile_id)) {
		profile_id 	= req.query.profile_id;
	}

	if (checkOptional(req.query.geo_cc)) {
		geo_cc 		= req.query.geo_cc;
	}

	if (checkOptional(req.query.geo_region)) {
		geo_region 	= req.query.geo_region;
	}

	if (checkOptional(req.query.geo_city)) {
		geo_city 	= req.query.geo_city;
	}

	if (checkOptional(req.query.width)) {
		width 		= req.query.width;
	}

	if (checkOptional(req.query.height)) {
		height 		= req.query.height;
	}

	/*
		Check config and get variables
	*/ 
	if (placements) {
		placements.forEach(function(element) {
			if (element.plc_id != null && element.plc_id == plc_id) {
				config_data = element;
			}
		});
	}
	// console.log("config_data =", config_data);

	if (config_data == null) {
		res.send(errorObj(4004, "The configuration doesn't exist for this placement ID."));
		return;
	}

	if (typeof config_data.ad_type != 'undefined' && config_data.ad_type) {
		c_ad_type 			= config_data.ad_type;
	}
	if (typeof config_data.adserver_domain != 'undefined' && config_data.adserver_domain) {
		c_adserver_domain 	= config_data.adserver_domain;
	}
	if (typeof config_data.adserver_type != 'undefined' && config_data.adserver_type) {
		c_adserver_type 	= config_data.adserver_type;
	}
	if (typeof config_data.adserver_endpoint != 'undefined' && config_data.adserver_endpoint) {
		c_adserver_endpoint = config_data.adserver_endpoint;
	}
	if (typeof config_data.adserver_zoneid != 'undefined' && config_data.adserver_zoneid) {
		c_adserver_zoneid 	= config_data.adserver_zoneid;
	}
	if (typeof config_data.dmp_type != 'undefined' && config_data.dmp_type) {
		c_dmp_type 			= config_data.dmp_type;
	}
	if (typeof config_data.dmp_hostname != 'undefined' && config_data.dmp_hostname) {
		c_dmp_hostname 		= config_data.dmp_hostname;
	}
	if (typeof config_data.c_dmp_port != 'undefined' && config_data.c_dmp_port) {
		c_dmp_port 			= config_data.c_dmp_port;
	}

	/*
		Request on ad server
	 */
	if (c_adserver_domain && c_adserver_endpoint && c_adserver_zoneid) {
		if (c_adserver_type == "castoola-adserver") {
			var url = "http://";
			var geo = "";

			url += c_adserver_domain;
			url += c_adserver_endpoint;
			url += "?zoneid=" + c_adserver_zoneid;

			if (geo_cc) {
				geo += "&geo_cc=" + geo_cc;
			}
			if (geo_region) {
				geo += "&geo_region=" + geo_region;
			}
			if (geo_city) {
				geo += "&geo_city=" + geo_city;
			}

			// console.log("url =", url);
			// console.log("geo =", geo);

            var res_adserver = await doRequest(url + geo);

            // console.log("url + geo", url + geo);
			// console.log("res_adserver", res_adserver.body);

			if (res_adserver.statusCode == 200) {
				/* Get data from adserver */ 
				var ret_ad 	= getDataFormAdServer(res, res_adserver, c_adserver_type);

				if (ret_ad.ad_served == true) {
					r.ad_served 		= ret_ad.ad_served;
					r.ad_type 			= c_ad_type;
					r.adserver_domain 	= c_adserver_domain;
					r.adserver_type 	= c_adserver_type;
					r.creative 			= ret_ad.creative;
					
					var im_tr = protocol + "://" + host + "/v1/ads/log-impression?publisher_id=" + publisher_id + 
																	"&placement_id="  + plc_id + 
																	"&campaign_id="   + r.creative.campaign_id + 
																	"&creative_id="   + r.creative.creative_id + 
																	"&client_id="     + client_id;
					if (profile_id) {
						im_tr += "&profile_id=" + profile_id;
					}
					if (geo_cc) {
						im_tr += "&geo_cc=" + geo_cc;
					}
					if (geo_region) {
						im_tr += "&geo_region=" + geo_region;
					}
					if (geo_city) {
						im_tr += "&geo_city=" + geo_city;
					}
					if (r.creative.impression_trackers) {
						im_tr += "&impression_tracker=" + encodeURIComponent(r.creative.impression_trackers);
					}

					r.creative.impression_trackers = new Array(im_tr);

					var cl_tr = protocol + "://" + host + "/v1/ads/log-click?publisher_id=" + publisher_id + 
																	"&placement_id=" + plc_id + 
																	"&campaign_id="  + r.creative.campaign_id + 
																	"&creative_id="  + r.creative.creative_id + 
																	"&client_id="    + client_id;
					if (profile_id) {
						cl_tr += "&profile_id=" + profile_id;
					}
					if (geo_cc) {
						cl_tr += "&geo_cc=" + geo_cc;
					}
					if (geo_region) {
						cl_tr += "&geo_region=" + geo_region;
					}
					if (geo_city) {
						cl_tr += "&geo_city=" + geo_city;
					}
					if (r.creative.click_trackers) {
						cl_tr += "&click_tracker=" + encodeURIComponent(r.creative.click_trackers);
					}

					r.creative.click_trackers = new Array(cl_tr);
					
					var creative_url_geo = "";
					if (geo.length > 0 && geo[0] == '&') {
						creative_url_geo = geo.replace('&','?');
					}
					r.creative.creative_url   = r.creative.creative_url + creative_url_geo;
				} else {
					r.ad_served = false;
				} 

			    if (type == "json" || type == "vast" || type == "script") {
			    	res.status(200);
					/* json */
					if (type == "json") {
						if (callback) {
							return jsonp(res, callback, r);
						} else {
							res.send(r);
							return;
						}
					/* vast TO-DO */
					} else if (type == "vast") {
						res.send(r);
						return;
					/* script TO-DO */
					} else if (type == "script") {
						res.send(r);
						return;
					}
				} else {
			    	res.send(errorObj(4002, "Response type is invalid or missing."));
					return;
			    }
			} else {
				res.send(errorObj(4006, "An unknown error has occurred."));
				return;
			}
		} else {
			res.send(errorObj(4005, "Wrong ad server configuration."));
			return;
		}
	} else {
		res.send(errorObj(4005, "Wrong ad server configuration."));
		return;
	}
};

/**
 * 
 * log-impression
 * log-click
 * log-conversion
 */
async function logImpression(req, res) { 
	/* Default response settings */
    res.setHeader('Content-Type', 'application/json');
	res.status(400);

	var publisher_id, placement_id, campaign_id, creative_id, client_id;
	var profile_id = " ", output_type = "px", callback, impression_tracker, geo_cc = " ", geo_region = " ", geo_city = " ";

	if (checkRequired(req.query.publisher_id)) {
		res.send(errorObj(5001, "Publisher ID is invalid or missing."));
		return;
	} else {
		publisher_id = req.query.publisher_id;
	}

	if (checkRequired(req.query.placement_id)) {
		res.send(errorObj(5002, "Placement ID is invalid or missing."));
		return;
	} else {
		placement_id = req.query.placement_id;
	}

	if (checkRequired(req.query.campaign_id)) {
		res.send(errorObj(5003, "Campaign ID is invalid or missing."));
		return;
	} else {
		campaign_id = req.query.campaign_id;
	}

	if (checkRequired(req.query.creative_id)) {
		res.send(errorObj(5004, "Creative ID is invalid or missing."));
		return;
	} else {
		creative_id = req.query.creative_id;
	}
	
	if (checkRequired(req.query.client_id)) {
		res.send(errorObj(5005, "Client ID is invalid or missing."));
		return;
	} else {
		client_id = req.query.client_id;

		if (!UUID.check(req.query.client_id)) {
			res.send(errorObj(5005, "Client ID is invalid or missing."));
			return;
		}
	}

	/* Optional */
	if (checkOptional(req.query.output_type)) {
		if (req.query.output_type === "px") {
			output_type = req.query.output_type;
		} else if (req.query.output_type === "json") {
			output_type = req.query.output_type;

			if (checkOptional(req.query.callback)) {
				output_type = "jsonp";
				callback    = req.query.callback;
			}
		} else {
			res.send(errorObj(5006, "Output type is invalid (json or px)."));
			return;
		}
	}

	if (checkOptional(req.query.profile_id)) {
		profile_id 	= req.query.profile_id;
	}

	if (checkOptional(req.query.impression_tracker)) {
		impression_tracker = req.query.impression_tracker;
	}

	if (checkOptional(req.query.geo_cc)) {
		geo_cc 		= req.query.geo_cc;
	}

	if (checkOptional(req.query.geo_region)) {
		geo_region  = req.query.geo_region;
	}

	if (checkOptional(req.query.geo_city)) {
		geo_city 	= req.query.geo_city;
	}

	var ts = Date.now();
	var params = {
		TableName: 'atv_dmp_v1_advertising',
		Item: {
			'client_id': 	{S: client_id},
			'ts':    		{S: ts.toString()},
			'action': 	 	{S: "impression"},
			'publisher_id': {S: publisher_id},
			'placement_id': {S: placement_id},
			'campaign_id': 	{S: campaign_id},
			'creative_id': 	{S: creative_id},
			'geo_cc': 		{S: geo_cc},
			'geo_region': 	{S: geo_region},
			'geo_city': 	{S: geo_city},
			'profile_id':   {S: profile_id}
		}
	};

	var imp = await putItem(ddb, params);
	if (imp.succeed === true) {
		res.status(200);
		if (output_type === "px") {
			return px(res, true, impression_tracker);
		} else if (output_type === "json") {
			return json(res, {success: "ok"},
				true,
				impression_tracker
			);
		} else {
			return jsonp(res, callback, {success: "ok"},
				true,
				impression_tracker
			);
		}

		
	} else {
		res.send(errorObj(5007, "An unknown error has occurred."));
		return;
	}
};

async function logClick(req, res) {
	/* Default response settings */
	res.setHeader('Content-Type', 'application/json');
	res.status(400);

	var publisher_id, placement_id, campaign_id, creative_id, client_id;
	var profile_id = " ", output_type = "px", callback, click_tracker = "", geo_cc = " ", geo_region = " ", geo_city = " ";

	if (checkRequired(req.query.publisher_id)) {
		res.send(errorObj(6001, "Publisher ID is invalid or missing."));
		return;
	} else {
		publisher_id = req.query.publisher_id;
	}

	if (checkRequired(req.query.placement_id)) {
		res.send(errorObj(6002, "Placement ID is invalid or missing."));
		return;
	} else {
		placement_id = req.query.placement_id;
	}

	if (checkRequired(req.query.campaign_id)) {
		res.send(errorObj(6003, "Campaign ID is invalid or missing."));
		return;
	} else {
		campaign_id = req.query.campaign_id;
	}

	if (checkRequired(req.query.creative_id)) {
		res.send(errorObj(6004, "Creative ID is invalid or missing."));
		return;
	} else {
		creative_id = req.query.creative_id;
	}
	
	if (checkRequired(req.query.client_id)) {
		res.send(errorObj(6005, "Client ID is invalid or missing."));
		return;
	} else {
		client_id = req.query.client_id;

		if (!UUID.check(req.query.client_id)) {
			res.send(errorObj(6005, "Client ID is invalid or missing."));
			return;
		}
	}

	/* Optional */
	if (checkOptional(req.query.output_type)) {
		if (req.query.output_type === "px") {
			output_type = req.query.output_type;
		} else if (req.query.output_type === "json") {
			output_type = req.query.output_type;

			if (checkOptional(req.query.callback)) {
				output_type = "jsonp";
				callback    = req.query.callback;
			}
		} else if (req.query.output_type === "redirection") {
			output_type = req.query.output_type;
		} else {
			res.send(errorObj(6006, "Output type is invalid (json, px or redirection)."));
			return;
		}
	}

	if (checkOptional(req.query.profile_id)) {
		profile_id 	  = req.query.profile_id;
	}

	if (checkOptional(req.query.click_tracker)) {
		click_tracker = req.query.click_tracker;
	}

	if (checkOptional(req.query.geo_cc)) {
		geo_cc 		  = req.query.geo_cc;
	}

	if (checkOptional(req.query.geo_region)) {
		geo_region    = req.query.geo_region;
	}

	if (checkOptional(req.query.geo_city)) {
		geo_city 	  = req.query.geo_city;
	}

	var ts = Date.now();
	var params = {
		TableName: 'atv_dmp_v1_advertising',
		Item: {
			'client_id': 	{S: client_id},
			'ts':    		{S: ts.toString()},
			'action': 	 	{S: "click"},
			'publisher_id': {S: publisher_id},
			'placement_id': {S: placement_id},
			'campaign_id': 	{S: campaign_id},
			'creative_id': 	{S: creative_id},
			'geo_cc': 		{S: geo_cc},
			'geo_region': 	{S: geo_region},
			'geo_city': 	{S: geo_city},
			'profile_id':   {S: profile_id}
		}
	};

	var clk = await putItem(ddb, params);
	if (clk.succeed === true) {
		if (output_type === "px") {
			return px(res, true, click_tracker);
		} else if (output_type === "redirection") {
			if (click_tracker.length < 1) {
				res.send(errorObj(6007, "An unknown error has occurred."));
				return;
			} else {
				return redirect(res, click_tracker);
			}
		} else if (output_type === "json") {
			return json(res, {success: "ok", destination: click_tracker},
				true,
				click_tracker);
		} else {
			return jsonp(res, callback, {success: "ok", destination: click_tracker},
				true,
				click_tracker
			);
		}
	} else {
		res.send(errorObj(6007, "An unknown error has occurred."));
		return;
	}
};

async function logConversion(req, res) {
	/* Default response settings */
    res.setHeader('Content-Type', 'application/json');
	res.status(400);

	var publisher_id, placement_id, campaign_id, creative_id, client_id;
	var profile_id = " ", output_type = "px", callback, conversion_tracker, geo_cc = " ", geo_region = " ", geo_city = " ";

	if (checkRequired(req.query.publisher_id)) {
		res.send(errorObj(7001, "Publisher ID is invalid or missing."));
		return;
	} else {
		publisher_id = req.query.publisher_id;
	}

	if (checkRequired(req.query.placement_id)) {
		res.send(errorObj(7002, "Placement ID is invalid or missing."));
		return;
	} else {
		placement_id = req.query.placement_id;
	}

	if (checkRequired(req.query.campaign_id)) {
		res.send(errorObj(7003, "Campaign ID is invalid or missing."));
		return;
	} else {
		campaign_id = req.query.campaign_id;
	}

	if (checkRequired(req.query.creative_id)) {
		res.send(errorObj(7004, "Creative ID is invalid or missing."));
		return;
	} else {
		creative_id = req.query.creative_id;
	}
	
	if (checkRequired(req.query.client_id)) {
		res.send(errorObj(7005, "Client ID is invalid or missing."));
		return;
	} else {
		client_id = req.query.client_id;

		if (!UUID.check(req.query.client_id)) {
			res.send(errorObj(7005, "Client ID is invalid or missing."));
			return;
		}
	}

	/* Optional */
	if (checkOptional(req.query.output_type)) {
		if (req.query.output_type === "px") {
			output_type = req.query.output_type;
		} else if (req.query.output_type === "json") {
			output_type = req.query.output_type;

			if (checkOptional(req.query.callback)) {
				output_type = "jsonp";
				callback    = req.query.callback;
			}
		} else {
			res.send(errorObj(7006, "Output type is invalid (json or px)."));
			return;
		}
	}

	if (checkOptional(req.query.profile_id)) {
		profile_id 	= req.query.profile_id;
	}

	if (checkOptional(req.query.conversion_tracker)) {
		conversion_tracker = req.query.conversion_tracker;
	}

	if (checkOptional(req.query.geo_cc)) {
		geo_cc 		= req.query.geo_cc;
	}

	if (checkOptional(req.query.geo_region)) {
		geo_region  = req.query.geo_region;
	}

	if (checkOptional(req.query.geo_city)) {
		geo_city 	= req.query.geo_city;
	}

	var ts = Date.now();
	var params = {
		TableName: 'atv_dmp_v1_advertising',
		Item: {
			'client_id': 	{S: client_id},
			'ts':    		{S: ts.toString()},
			'action': 	 	{S: "conversion"},
			'publisher_id': {S: publisher_id},
			'placement_id': {S: placement_id},
			'campaign_id': 	{S: campaign_id},
			'creative_id': 	{S: creative_id},
			'geo_cc': 		{S: geo_cc},
			'geo_region': 	{S: geo_region},
			'geo_city': 	{S: geo_city},
			'profile_id':   {S: profile_id}
		}
	};

	var cnv = await putItem(ddb, params);
	if (cnv.succeed === true) {
		if (output_type === "px") {
			return px(res, true, conversion_tracker);
		} else if (output_type === "json") {
			return json(res, {success: "ok"},
				true,
				conversion_tracker
			);
		} else {
			return jsonp(res, callback, {success: "ok"},
				true,
				conversion_tracker
			);
		}
	} else {
		res.send(errorObj(7007, "An unknown error has occurred."));
		return;
	}
};


/**
 * 
 * Functions
 */
async function getUuid(name) {
	/* return new promise */ 
	return new Promise(function(resolve, reject) {
        /* do async job */
        UUID.v5({
	    	namespace: UUID.namespace.cstm, /* url */
	    	name:      name
		}, function (err, result) {
	      	if (err === null) {
	        	resolve(result);
	      	} else {
	        	reject(result);
	      	}
	    });
    })  
};

async function getItem(ddb, item) {
	/* return new promise */ 
	return new Promise(function(resolve, reject) {
        try {
        	ddb.query(item, function(err, data) {
			    if (err) {
					console.log(err); reject({succeed: false});
			    } else {
			    	resolve({data: data.Items});
			    }
			});
        } catch (e) {
			reject({succeed: false});
		}
    })
};

async function putItem(ddb, item) {
	/* return new promise */ 
	return new Promise(function(resolve, reject) {
        try {
        	ddb.putItem(item, function(err, data) {
			    if (err) {
					console.log(err);
					reject({succeed: false, error: err});
				}
			    else {
					resolve({succeed: true});
				}
			});
        } catch (e) {
			console.log(e);
			reject({succeed: false, error: e});
		}
    })
};

async function updateItem(ddb, item) {
	/* return new promise */ 
	return new Promise(function(resolve, reject) {
        try {
        	ddb.updateItem(item, function(err, data) {
			    if (err) {
					console.log(err); reject({succeed: false});
				} else
			    	resolve({succeed: true});
			});
        } catch (e) {
			reject({succeed: false});
		}
    })
};

async function doRequest(url) {
    /* return new promise */ 
    return new Promise(function(resolve, reject) {
        /* do async job */
        request(url, function(error, response, body) { 
        	if (!error && response.statusCode == 200) {
				console.log("[log] doRequest", url);
	        	resolve(response);
	      	} else {
				console.log("[log] doRequest", error);
	        	reject(error);
	      	}
        })
    });
};

/*async function getGeo(ip_address) {
	var url = "http://microservices.castoola.tv/geo/index.php?ip="+ip_address; 
	var url = "http://54.93.53.71/geo/index.php?ip="+ip_address; 
	
    /* return new promise */ 
    // return new Promise(function(resolve, reject) {
        /* do async job */
        /*request(url, function(error, response, body) { 
        	if (!error && response.statusCode == 200) {
				resolve(response.body);
			  } else {
				console.log("[error] = ", error);
	        	reject(error);
	      	}
        })
    });
};*/

function getVersion(ua) {
	ua = ua.toLowerCase();
	var version = "unknown";

	if (ua.indexOf("hbbtv/1.1.1") !== -1) {
        version = "HbbTV/1.1";
    } else if (ua.indexOf("hbbtv/1.2.1") !== -1) {
        version = "HbbTV/1.5";
    } else if (ua.indexOf("hbbtv/1.3.1") !== -1) {
        version = "HbbTV/2.0";
    } else if (ua.indexOf("hbbtv/1.4.1") !== -1) {
        version = "HbbTV/2.0.1";
    } else if (ua.indexOf("hbbtv/1.5.1") !== -1) {
        version = "HbbTV/2.0.2";
    } 

    return version;
};

function getBrand(ua) {
	ua = ua.toLowerCase();
	var brand = "unknown";
	var brands = ["samsung", "philips", "sony", "lge", "panasonic", "toshiba", "vestel", "jvc", "firetv", "hisense", "sharp", "mstar", "tcl", "tpvision", "tesla", "grundig", "medion", "hitachi", "hyundai", "manta", "orion", "nec"];

	brands.forEach(function(b) {
		if (ua.indexOf(b) !== -1) {
	        brand = b;
	        return brand;
	    }
	});

    return brand;
};

function getDataFormAdServer(res, data, adserver_type) {
	var ret = {};
	ret.creative = {};
	if (adserver_type == "castoola-adserver") {
		var json = JSON.parse(data.body);
		if (json.ad_served == true) {
			ret.ad_served 						= true;
			ret.creative.campaign_id 			= json.campaign_id   ? json.campaign_id 	  : "";
			ret.creative.pricing_model          = json.pricing_model ? pricing_model 		  : "";
			ret.creative.currency               = json.currency 	 ? currency 			  : "";
			ret.creative.price  				= json.price         ? parseFloat(json.price) : 0.0;
			ret.creative.creative_id   			= json.creative_id   ? json.creative_id       : "";
			ret.creative.creative_url    		= json.imageUrl      ? json.imageUrl          : "";
			ret.creative.width    				= json.width         ? parseInt(json.width)   : 0;
			ret.creative.height    				= json.height        ? parseInt(json.height)  : 0;
			ret.creative.impression_trackers	= new Array(json.impressionUrl);
			ret.creative.click_trackers			= new Array(json.clickUrl);
			ret.creative.conversion_trackers	= new Array(json.conversionUrl);
		} else {
			ret.creative.ad_served = false;
		}
	}

	return ret;
};

function checkRequired(param) {
	if (typeof param === "undefined" || param === null || param === "") {
		return true;
	}
	return false;
};

function checkOptional(param) {
	if (typeof param !== "undefined" && param !== null && param !== "") {
		return true;
	}
	return false;
};

function isInteger(num) {
	var reg = /^\d+$/;
	if (typeof num !== "undefined") {
    	if (reg.test(num)) {
    		num = parseInt(num);
    		if (Number.isInteger(num))
				return true;
    	}
	}
	return false;
};

function getRndInteger(min, max) {
	return Math.floor(Math.random() * (max - min + 1) ) + min;
}

function errorObj(error_code, message) {
	return {
    	error_code: error_code,
      	message:    message
	};
};

function json(res, data) {
	res.status(200);
	res.send(data);
    return;
};

async function json(res, data, do_request, url) {
	res.status(200);
	res.send(data);

	if (do_request && url.length > 0) {
		await doRequest(url);
	}

    return;
};

async function jsonp(res, callback, data) {
	res.status(200);
	res.setHeader('Content-Type', 'application/javascript');
	res.send(
		callback + '(' + JSON.stringify(data) + ')'
	);
	return;
};

async function jsonp(res, callback, data, do_request, url) {
	res.status(200);
	res.setHeader('Content-Type', 'application/javascript');
	res.send(
		callback + '(' + JSON.stringify(data) + ')'
	);

	if (do_request && url.length > 0) {
		await doRequest(url);
	}

	return;
};

async function px(res) {
	res.status(200);
	res.set('Access-Control-Allow-Origin', '*');
	res.set('Content-Type', 'image/gif');
	res.set('isBase64Encoded', true);
	res.send(imageHex);
	return;
};

async function px(res, do_request, url) {
	res.status(200);
	res.set('Access-Control-Allow-Origin', '*');
	res.set('Content-Type', 'image/gif');
	res.set('isBase64Encoded', true);
	res.send(imageHex);

	if (do_request && url.length > 0) {
		await doRequest(url);
	}

	return;
};

function redirect(res, url) {
	res.redirect(302, url);
	return;
};
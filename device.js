/* Load the AWS SDK for Node.js */
const AWS        = require('aws-sdk');
const request    = require('request');
const UUID       = require('/opt/bitnami/apache2/htdocs/atv-dmp/uuid.js');
const crypto     = require('crypto');
const placements = require('/opt/bitnami/apache2/htdocs/atv-dmp/config.json');

AWS.config.update({
    accessKeyId: 	 'AKIAJ2HE4GU3YAO472CA',
    secretAccessKey: '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',
    region: 		 'eu-central-1'
});

const s3         = new AWS.S3();
const dynamodb   = new AWS.DynamoDB();
const lambda     = new AWS.Lambda();
const docClient  = new AWS.DynamoDB.DocumentClient();

const cron    = require("node-cron");
const express = require("express");
const cluster = require('cluster');
const fs      = require("fs");
const os      = require('os');

app = express();

var ddb = new AWS.DynamoDB({
    apiVersion: '2012-08-10'
});

// Code to run if we're in the master process
if (cluster.isMaster) {
    var cpuCount = os.cpus().length;

    for (var i = 0; i < cpuCount; i += 1) {
        cluster.fork();
    }

    cluster.on('exit', function (worker) {
        console.log({
        	worker_id: worker.id,
        	event: "exit"
        });

        cluster.fork();
    });
// Code to run if we're in a worker process
} else {
	var cpuCount = os.cpus().length;

    app.get('/device-login', function (req, res) {    	
    	deviceLogin(req, res);
    });

    var server = app.listen(3005, function () {
	  	var host = server.address().address;
	    var port = server.address().port;
	    console.log("[castoola-atv-dmp] device-app listening at http://%s:%s", host, port);
	});
}

async function deviceLogin(req, res) {
	/* Default response settings */
    res.setHeader('Content-Type', 'application/json');
    res.status(400);

    /* init */
	var qs, callback, client, clients, audience, audiences;
	var device_id   = "0";
	var profile_id  = "0";
	var output_type = "json";
	var d_id_exists = false;
    var p_id_exists = false;
    
    /* Required */
    if (checkRequired(req.query.service_id)) {
        res.send(errorObj(1001, "Service ID is invalid or missing."));
		return;
    }
    
    if (checkRequired(req.query.platform)) {
        res.send(errorObj(1002, "Platform is invalid or missing."));
		return;
    }
    
    /* Optional */
	if (checkOptional(req.query.device_id)) {
		d_id_exists = true;
		device_id   = req.query.device_id;
	}

	if (checkOptional(req.query.profile_id)) {
		p_id_exists = true;
		profile_id  = req.query.profile_id;
	}

	if (checkOptional(req.query.callback)) {
		output_type = "jsonp";
		callback    = req.query.callback;
    }
    
    var ip         = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
        ip         = ip.replace(/^.*:/, '');
    var ua         = req.headers['user-agent']; 								
    var ip_address = crypto.createHash('md5').update(ip).digest("hex");
    var cid_name   = ip.concat(ua);    
    
	if (d_id_exists === true) cid_name = cid_name.concat(device_id);
	if (p_id_exists === true) cid_name = cid_name.concat(profile_id);

	var client_id = await getUuid(cid_name);

	/* Get location data */
	var location;
	try {
		location = await getGeo(ip, "K7GznJVRAq57Gp7E");    
    	location = JSON.parse(location);
	} catch (e) {
		res.send(errorObj(1003, "An unknown error has occurred."));
		return;
    }
    
    console.log("client_id =", client_id);

    var params = {
	    TableName : "atv_dmp_v1_clients",
	    ExpressionAttributeValues: {
		    ':c_id': {S: client_id}
		},
		KeyConditionExpression: 'client_id = :c_id'
    };	
    
	var client_login = await getItem(ddb, params);

	if (client_login.data.length > 0) {
        client = {
            TableName: 'atv_dmp_v1_clients',
            Key:{
                'client_id': {S: client_id}
            },
            UpdateExpression: "set brand = :b, device_id = :did, geo_cc = :gcc, geo_city = :gc, geo_region = :gr, geo_zip = :gz, ip_address = :ipa, version = :v",
            ExpressionAttributeValues:{
                ':b'  : {S: getBrand(ua)},
                ':did': {S: device_id},
                ':gcc': {S: location.geo_cc},
                ':gr' : {S: location.geo_region},
                ':gc' : {S: location.geo_city},
                ':gz' : {S: location.geo_zip},
                ':ipa': {S: ip_address},
                ':v'  : {S: getVersion(ua)}
            },
        };

        if (p_id_exists) {
            client.ConditionExpression = 'profile_id = :pid';
            client.ExpressionAttributeValues[':pid'] = {S: profile_id};
        }

		clients = await updateItem(ddb, client);
	} else {
        client = {
            TableName: 'atv_dmp_v1_clients',
            Item: {
                'client_id' : {S: client_id},
                'brand'     : {S: getBrand(ua)},
                'device_id' : {S: device_id},
                'geo_cc'    : {S: location.geo_cc},
                'geo_region': {S: location.geo_region},
                'geo_city'  : {S: location.geo_city},
                'geo_zip'   : {S: location.geo_zip},
                'ip_address': {S: ip_address},
                'version'   : {S: getVersion(ua)}
            }
        };

        if (p_id_exists) {
            client.Item['profile_id'] = {S: profile_id};
        }

		clients = await putItem(ddb, client);
    }

	/* Audience */
	var ts = Date.now();
	audience = {
		TableName: 'atv_dmp_v1_audience',
		Item: {
			'client_id' : {S: client_id},
			'ts'        : {S: ts.toString()},
			'brand'     : {S: getBrand(ua)},
			'device_id' : {S: device_id},
			'geo_cc'    : {S: location.geo_cc},
			'geo_region': {S: location.geo_region},
			'geo_city'  : {S: location.geo_city},
			'geo_zip'   : {S: location.geo_zip},
			'ip_address': {S: ip_address},
			'platform'  : {S: req.query.platform},
			'service_id': {S: req.query.service_id},
			'ua'        : {S: ua},
            'version'   : {S: getVersion(ua)}
		}
    };

    try {
        audiences = await putItem(ddb, audience);
    } catch(e) {
        console.log(e.message);
    }

	if (clients.succeed === true && audiences.succeed === true) {
		var data = {
			client_id:    client_id,
			geo_cc:       location.geo_cc,
			geo_region:   location.geo_region,
			geo_city:     location.geo_city,
			geo_zip:      location.geo_zip,
			geo_province: location.geo_province
        };
    
        res.status(200);
		if (output_type === "json") {
            res.send(JSON.stringify(data));
            return;
		} else {            
            return jsonp(res, callback, data);
		}
	} else {
        res.send(errorObj(1003, "An unknown error has occurred."));
		return;
    }
};

/*
	UUID
 */
async function getUuid(name) {
	/* return new promise */ 
	return new Promise(function(resolve, reject) {
        /* do async job */
        UUID.v5({
	    	namespace: UUID.namespace.cstm, /* url */
	    	name:      name
		}, function (err, result) {
	      	if (err === null) {
	        	resolve(result);
	      	} else {
	        	reject(result);
	      	}
	    });
    })  
};

/*
	S3 bucket functions
 */
async function getItem(ddb, item) {
	/* return new promise */ 
	return new Promise(function(resolve, reject) {
        try {
        	ddb.query(item, function(err, data) {
			    if (err) {
			        reject({succeed: false});
			    } else {
			    	resolve({data: data.Items});
			    }
			});
        } catch (e) {
			reject({succeed: false});
		}
    })
};

async function putItem(ddb, item) {
	/* return new promise */ 
	return new Promise(function(resolve, reject) {
        try {
        	ddb.putItem(item, function(err, data) {
			    if (err)
                    reject({succeed: false});
			    else
			    	resolve({succeed: true});
			});
        } catch (e) {
			reject({succeed: false});
		}
    })
};

async function updateItem(ddb, item) {
	/* return new promise */ 
	return new Promise(function(resolve, reject) {
        try {
        	ddb.updateItem(item, function(err, data) {
			    if (err)
			        reject({succeed: false});
			    else
			    	resolve({succeed: true});
			});
        } catch (e) {
			reject({succeed: false});
		}
    })
};

/*
	Default doRequest
 */
async function doRequest(url) {
    /* return new promise */ 
    return new Promise(function(resolve, reject) {
        /* do async job */
        request(url, function(error, response, body) { 
        	if (!error && response.statusCode == 200) {
	        	resolve(response);
	      	} else {
	        	reject(error);
	      	}
        })
    });
};

/*
	Geolocation do request (microservice)
 */
async function getGeo(ip_address, access_key) {
    var url = "http://3.121.209.57/geo.php?ip="+ip_address+"&access_key="+access_key;
    /* return new promise */ 
    return new Promise(function(resolve, reject) {
        /* do async job */
        request(url, function(error, response, body) { 
        	if (!error && response.statusCode == 200) {
	        	resolve(response.body);
	      	} else {
	        	reject(error);
	      	}
        })
    });
};

/*
	Other functions
 */
function getVersion(ua) {
	ua = ua.toLowerCase();
	var version = "unknown";

	if (ua.indexOf("hbbtv/1.1.1") !== -1) {
        version = "HbbTV/1.1";
    } else if (ua.indexOf("hbbtv/1.2.1") !== -1) {
        version = "HbbTV/1.5";
    } else if (ua.indexOf("hbbtv/1.3.1") !== -1) {
        version = "HbbTV/2.0";
    } else if (ua.indexOf("hbbtv/1.4.1") !== -1) {
        version = "HbbTV/2.0.1";
    } else if (ua.indexOf("hbbtv/1.5.1") !== -1) {
        version = "HbbTV/2.0.2";
    } 

    return version;
};

function getBrand(ua) {
	ua = ua.toLowerCase();
	var brand = "unknown";
	var brands = ["samsung", "philips", "sony", "lge", "panasonic", "toshiba", "vestel", "jvc", "firetv", "hisense", "sharp", "mstar", "tcl", "tpvision", "tesla", "grundig", "medion", "hitachi", "hyundai", "manta", "orion", "nec"];

	brands.forEach(function(b) {
		if (ua.indexOf(b) !== -1) {
	        brand = b;
	        return brand;
	    }
	});

    return brand;
};

function checkRequired(param) {
	if (typeof param === "undefined" || param === null || param === "") {
		return true;
	}
	return false;
};

function checkOptional(param) {
	if (typeof param !== "undefined" && param !== null && param !== "") {
		return true;
	}
	return false;
};

function errorObj(error_code, message) {
	return {
    	error_code: error_code,
      	message:    message
	};
};

function jsonp(res, callback, data) {
	res.setHeader('Content-Type', 'application/javascript');
	res.send(
		callback + '(' + JSON.stringify(data) + ')'
	);
	return;
};
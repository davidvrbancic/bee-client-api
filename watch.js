/* Load the AWS SDK for Node.js */
const AWS     = require('aws-sdk');
const request = require('request');
const UUID    = require('/opt/bitnami/apache2/htdocs/atv-dmp/uuid.js');
const crypto  = require('crypto');

AWS.config.update({
    // accessKeyId: 	'AKIAJO7GHRYJEDD7WKZQ',
    // secretAccessKey: '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',
    accessKeyId: 	 'AKIAJ2HE4GU3YAO472CA',
    secretAccessKey: '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',
    region: 		 'eu-central-1'
});

const s3         = new AWS.S3();
const dynamodb   = new AWS.DynamoDB();
const lambda     = new AWS.Lambda();
const docClient  = new AWS.DynamoDB.DocumentClient();

/* const cron    = require("node-cron"); */
const express = require("express");
const cluster = require('cluster');
/* const fs      = require("fs"); */
const os      = require('os');

app = express();

var ddb = new AWS.DynamoDB({
    apiVersion: '2012-08-10'
});

/* Code to run if we're in the master process */
if (cluster.isMaster) {
    var cpuCount = os.cpus().length;

    for (var i = 0; i < cpuCount; i += 1) {
        cluster.fork();
    }

    cluster.on('exit', function (worker) {
        console.log({
        	worker_id: worker.id,
        	event: "exit"
        });

        cluster.fork();
    });
/* Code to run if we're in a worker process */
} else {
    app.get('/set-watch', function (req, res) {    	
    	setWatch(req, res);
    });

    app.get('/get-watch', function (req, res) {    	
    	getWatch(req, res);
    });

    var server = app.listen(3001, function () {
	  	var host = server.address().address;
	    var port = server.address().port;
	    console.log("[castoola-atv-dmp] watch-app listening at http://%s:%s", host, port);
	});
}

async function setWatch(req, res) {
	/* Default response settings */
    res.setHeader('Content-Type', 'application/javascript');
	res.status(200);

	var client_id, media_id, callback = "setWatch";

	/* Check params - Required */
	if (req.query.client_id) {
		client_id = req.query.client_id;

		if (!UUID.check(client_id)) {
			return jsonp(res, callback, 
				errorObj(1002, "Client ID is invalid.")
			);
		}
	} else { 
		return jsonp(res, callback, 
			errorObj(1001, "Client ID is invalid or missing.")
		);
	}

	if (req.query.media_id) {
		media_id = req.query.media_id;
	} else { 
		return jsonp(res, callback, 
			errorObj(1003, "Media ID is invalid or missing.")
		);
	}

	if (req.query.callback) {
		callback = req.query.callback;
	} else { 
		return jsonp(res, callback, 
			errorObj(1004, "Callback is invalid or missing.")
		);
	}

	var ts = Date.now();
	var params = {
		TableName: 'atv_dmp_v1_watch',
		Item: {
			'client_id': {S: client_id},
			'ts' : 		 {S: ts.toString()},
			'media_id':  {S: media_id}
		}
	};

	var watch = await putItem(ddb, params);
	if (watch.succeed === true) {
		return jsonp(res, callback, {
			success: "ok"
		});
	} else {
		return jsonp(res, callback, 
			errorObj(1005, "An unknown error has occurred.")
		);
	}
};

async function getWatch(req, res) {
	/* Default response settings */
    res.setHeader('Content-Type', 'application/javascript');
	res.status(200);

	var client_id, media_id, callback = "getWatch";

	/* Check params - Required */
	if (req.query.client_id) {
		client_id = req.query.client_id;

		if (!UUID.check(client_id)) {
			return jsonp(res, callback, 
				errorObj(1002, "Client ID is invalid.")
			);
		}
	} else { 
		return jsonp(res, callback, 
			errorObj(1001, "Client ID is invalid or missing.")
		);
	}

	if (req.query.media_id) {
		media_id = req.query.media_id;
	} else { 
		return jsonp(res, callback, 
			errorObj(1003, "Media ID is invalid or missing.")
		);
	}

	if (req.query.callback) {
		callback = req.query.callback;
	} else {
		return jsonp(res, callback, 
			errorObj(1004, "Callback is invalid or missing.")
		);
	}

	var ts = Date.now();
	var params = {
	    TableName : "atv_dmp_v1_watch",
		KeyConditionExpression: '#c_id = :c_id',
		FilterExpression: "#m_id = :m_id",
		ExpressionAttributeNames:{
        	"#c_id": "client_id",
        	"#m_id": "media_id"
    	},
	    ExpressionAttributeValues: {
		    ':c_id': {S: client_id},
		    ':m_id': {S: media_id}
		},
	};

	var watch = await getItem(ddb, params);

	if (watch.data.length > 0) {
		return jsonp(res, callback, {
			is_watching: true,
			success:     "ok"
		});
	}

	return jsonp(res, callback, {
		is_watching: false,
		success:     "ok"
	});
};

async function getItem(ddb, item) {
	/* return new promise */ 
	return new Promise(function(resolve, reject) {
        try {
        	ddb.query(item, function(err, data) {
			    if (err) {
			        reject({succeed: false});
			    } else {
			    	resolve({data: data.Items});
			    }
			});
        } catch (error) {
			reject({succeed: false});
		}
    })
};

async function putItem(ddb, item) {
	/* return new promise */ 
	return new Promise(function(resolve, reject) {
        try {
        	ddb.putItem(item, function(err, data) {
			    if (err)
			        reject({succeed: false});
			    else
			    	resolve({succeed: true});
			});
        } catch (error) {
			reject({succeed: false});
		}
    })
};

function errorObj(error_code, message) {
	return {
    	error_code: error_code,
      	message:    message
	};
};

function jsonp(res, callback, data) {
	res.send(
		callback + '(' + JSON.stringify(data) + ')'
	);
	return;
};
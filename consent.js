/* Load the AWS SDK for Node.js */
const AWS     = require('aws-sdk');
const request = require('request');
const UUID    = require('/opt/bitnami/apache2/htdocs/atv-dmp/uuid.js');
const crypto  = require('crypto');

AWS.config.update({
    accessKeyId: 	 'AKIAJ2HE4GU3YAO472CA',
    secretAccessKey: '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',
    region: 		 'eu-central-1'
});

const s3         = new AWS.S3();
const dynamodb   = new AWS.DynamoDB();
const lambda     = new AWS.Lambda();
const docClient  = new AWS.DynamoDB.DocumentClient();

const cron    = require("node-cron");
const express = require("express");
const cluster = require('cluster');
const fs      = require("fs");
const os      = require('os');

app = express();

var ddb = new AWS.DynamoDB({
    apiVersion: '2012-08-10'
});

// Code to run if we're in the master process
if (cluster.isMaster) {
    var cpuCount = os.cpus().length;

    for (var i = 0; i < cpuCount; i += 1) {
        cluster.fork();
    }

    cluster.on('exit', function (worker) {
        console.log({
        	worker_id: worker.id,
        	event: "exit"
        });

        cluster.fork();
    });
// Code to run if we're in a worker process
} else {
	var cpuCount = os.cpus().length;

    app.get('/get-consent', function (req, res) {    	
    	getConsent(req, res);
    });

    app.get('/set-consent', function (req, res) {    	
    	setConsent(req, res);
	});

    var server = app.listen(3002, function () {
	  	var host = server.address().address;
	    var port = server.address().port;
	    console.log("[castoola-atv-dmp] consent-app listening at http://%s:%s", host, port);
	});
}

async function getConsent(req, res) {
	/* Default response settings */
    res.setHeader('Content-Type', 'application/json');
	res.status(400);

	/* init */
	var qs, callback, client, clients;
	var device_id   = "0";
	var profile_id  = "0";
	var output_type = "json";
	var d_id_exists = false;
	var p_id_exists = false;
	var consent     = false;

	var publisher_id, service_id, platform, consent_key, consent_value;

	/* Check params - Required */
	if (req.query.publisher_id) {
		publisher_id = req.query.publisher_id;
	} else { 
		res.send(errorObj(8001, "Service ID is invalid or missing."));
		return;
	}

	if (req.query.service_id) {
		service_id = req.query.service_id;
	} else { 
		res.send(errorObj(8002, "An unknown error has occurred."));
		return;
	}

	if (req.query.platform) {
		platform = req.query.platform;
	} else { 
		res.send(errorObj(8003, "An unknown error has occurred."));
		return;
	}

	if (req.query.consent_key) {
		consent_key = req.query.consent_key;
	} else { 
		res.send(errorObj(8004, "An unknown error has occurred."));
		return;
	}

	if (req.query.consent_value) {
		/* consent_value = req.query.consent_value; */

		if (req.query.consent_value !== "true" && req.query.consent_value !== "false") {	
			res.send(errorObj(8006, "An unknown error has occurred."));
			return;
		}
	} else { 
		res.send(errorObj(8005, "An unknown error has occurred."));
		return;
	}

	if (req.query.device_id) {
		d_id_exists = true;
		device_id   = req.query.device_id;
	}

	if (req.query.profile_id) {
		p_id_exists = true;
		profile_id  = req.query.profile_id;
	}

	if (req.query.callback) {
		output_type = "jsonp";
		callback    = req.query.callback;
	}

	var ip         = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
		ip         = ip.replace(/^.*:/, '');
	var ua         = req.headers['user-agent']; 								
	var ip_address = crypto.createHash('md5').update(ip).digest("hex");
	var cid_name   = ip.concat(ua);

	if (d_id_exists === true) cid_name = cid_name.concat(device_id);
	if (p_id_exists === true) cid_name = cid_name.concat(profile_id);
	
	var client_id = await getUuid(cid_name);

	/* Get location data */
	var location;
	try {
		location = await getGeo(ip, "K7GznJVRAq57Gp7E");    
    	location = JSON.parse(location);
	} catch (e) {
		res.send(errorObj(8006, "An unknown error has occurred."));
		return;
	}

	console.log("client_id =", client_id);

	var params = {
	    TableName : "atv_dmp_v1_clients",
	    ExpressionAttributeValues: {
		    ':c_id': {S: client_id}
		},
		KeyConditionExpression: 'client_id = :c_id'
	};

	var client_consent = await getItem(ddb, params);
	if (client_consent.data.length > 0) {
		try {
			if (typeof client_consent.data[0][consent_key]["S"] !== "undefined") {
				consent = client_consent.data[0][consent_key]["S"];
			}
		} catch (e) {
			res.send(errorObj(8006, "An unknown error has occurred."));
			return;
		}
	}

	if (consent === "true") consent = true;

	if (consent == true) {
		if (p_id_exists) {
			client = {
	    		TableName: 'atv_dmp_v1_clients',
			    Key:{
					'client_id': {S: client_id}
			    },
			    UpdateExpression: "set brand = :b, device_id = :did, geo_cc = :gcc, geo_city = :gc, geo_region = :gr, geo_zip = :gz, ip_address = :ipa, version = :v",
			    ConditionExpression: 'profile_id = :pid',
			    ExpressionAttributeValues:{
			    	':pid': {S: profile_id},
			        ':b'  : {S: getBrand(ua)},
			        ':did': {S: device_id},
			        ':gcc': {S: location.geo_cc},
			        ':gr' : {S: location.geo_region},
			        ':gc' : {S: location.geo_city},
			        ':gz' : {S: location.geo_zip},
			        ':ipa': {S: ip_address},
			        ':v'  : {S: getVersion(ua)}
			    },
			};
		} else {
			client = {
	    		TableName: 'atv_dmp_v1_clients',
			    Key:{
					'client_id': {S: client_id}
			    },
			    UpdateExpression: "set brand = :b, device_id = :did, geo_cc = :gcc, geo_city = :gc, geo_region = :gr, geo_zip = :gz, ip_address = :ipa, version = :v",
			    ExpressionAttributeValues:{
			        ':b'  : {S: getBrand(ua)},
			        ':did': {S: device_id},
			        ':gcc': {S: location.geo_cc},
			        ':gr' : {S: location.geo_region},
			        ':gc' : {S: location.geo_city},
			        ':gz' : {S: location.geo_zip},
			        ':ipa': {S: ip_address},
			        ':v'  : {S: getVersion(ua)}
			    },
			};
		}

		clients = await (updateItem(ddb, client));
	}	

	if (consent == true) {
		/* Audience */
		var ts = Date.now();
		client = {
			TableName: 'atv_dmp_v1_audience',
			Item: {
				'client_id'   : {S: client_id},
				'ts'          : {S: ts.toString()},
				'brand'       : {S: getBrand(ua)},
				'device_id'   : {S: device_id},
				'geo_cc'      : {S: location.geo_cc},
				'geo_region'  : {S: location.geo_region},
				'geo_city'    : {S: location.geo_city},
				'geo_zip'     : {S: location.geo_zip},
				'ip_address'  : {S: ip_address},
				'platform'    : {S: platform},
				'publisher_id': {S: publisher_id},
				'service_id'  : {S: service_id},
				'ua'          : {S: ua},
				'version'     : {S: getVersion(ua)}
			}
		};

		var audience = await putItem(ddb, client);

		if (clients.succeed === true && audience.succeed === true) {
			var data = {
				client_id: 	  client_id,
				geo_cc: 	  location.geo_cc,
				geo_region:   location.geo_region,
				geo_city: 	  location.geo_city,
				geo_zip: 	  location.geo_zip,
				geo_province: location.geo_province
			};

			res.status(200);
			if (output_type === "json") {
				res.send(JSON.stringify(data));
			} else {
				res.setHeader('Content-Type', 'application/javascript');
				res.send(
					callback + '(' + JSON.stringify(data) + ')'
				);
			}
			return;
		} else {
			res.send(errorObj(1003, "An unknown error has occurred."));
			return;
		}
	} else {
		var data = {};

		res.status(200);
		if (output_type === "json") {
			res.send(JSON.stringify(data));
		} else {
			res.setHeader('Content-Type', 'application/javascript');
			res.send(
				callback + '(' + JSON.stringify(data) + ')'
			);
		}
		return;
	}
};

async function setConsent(req, res) {
	/* Default response settings */
    res.setHeader('Content-Type', 'application/json');
	res.status(400);

	/* init */
	var qs, callback, client, clients;
	var device_id   = "0";
	var profile_id  = "0";
	var output_type = "json";
	var d_id_exists = false;
	var p_id_exists = false;

	
	/* Check params / Required */
	if (checkRequired(req.query.publisher_id)) {
		res.send(errorObj(1001, "An unknown error has occurred."));
		return;
	}

	if (checkRequired(req.query.platform)) {
		res.send(errorObj(1001, "An unknown error has occurred."));
		return;
	}

	if (checkRequired(req.query.consent_key)) {
		res.send(errorObj(1001, "An unknown error has occurred."));
		return;
	}

	if (checkRequired(req.query.consent_value)) {
		res.send(errorObj(1001, "An unknown error has occurred."));
		return;
	} else {
		if (!(req.query.consent_value == "true" || req.query.consent_value == "false")) {	
			res.send(errorObj(1001, "An unknown error has occurred."));
			return;
		}
	}

	/* Check params / Optional */
	if (checkOptional(req.query.device_id)) {
		d_id_exists = true;
		device_id   = req.query.device_id;
	}

	if (checkOptional(req.query.profile_id)) {
		p_id_exists = true;
		profile_id  = req.query.profile_id;
	}

	if (checkOptional(req.query.profile_id)) {
		p_id_exists = true;
		profile_id  = req.query.profile_id;
	}

	if (checkOptional(req.query.callback)) {
		output_type = "jsonp";
		callback    = req.query.callback;
	}

	var ip         = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
		ip         = ip.replace(/^.*:/, '');
	var ua         = req.headers['user-agent']; 								
	var ip_address = crypto.createHash('md5').update(ip).digest("hex");
	var cid_name   = ip.concat(ua);

	if (d_id_exists === true) cid_name = cid_name.concat(device_id);
	if (p_id_exists === true) cid_name = cid_name.concat(profile_id);

	var client_id = await getUuid(cid_name);

	console.log("client_id =", client_id);

	/* Get location data */
	var location;
	try {
		location = await getGeo(ip, "K7GznJVRAq57Gp7E");    
    	location = JSON.parse(location);
	} catch (e) {
		res.send(errorObj(1006, "An unknown error has occurred."));
		return;
	}

	var params = {
	    TableName : "atv_dmp_v1_clients",
	    ExpressionAttributeValues: {
		    ':c_id': {S: client_id}
		},
		KeyConditionExpression: 'client_id = :c_id'
	};

	var get_client = await getItem(ddb, params);

	if (get_client.data.length > 0) {
		if (req.query.consent_value == "true") {
			client = {
	    		TableName: 'atv_dmp_v1_clients',
			    Key:{
					'client_id' : {S: client_id}
			    },
			    UpdateExpression: "set brand = :b, device_id = :did, geo_cc = :gcc, geo_city = :gc, geo_region = :gr, geo_zip = :gz, ip_address = :ipa, version = :v, " + req.query.consent_key + " = :ck",
			    ExpressionAttributeValues:{
			        ':b'     : {S: getBrand(ua)},
			        ':did'   : {S: device_id},
			        ':gcc'   : {S: location.geo_cc},
			        ':gr'    : {S: location.geo_region},
			        ':gc'    : {S: location.geo_city},
			        ':gz'    : {S: location.geo_zip},
			        ':ipa'   : {S: ip_address},
			        ':v'     : {S: getVersion(ua)},
			        ':ck'    : {S: req.query.consent_value}
			    }
			};
		} else {
			client = {
	    		TableName: 'atv_dmp_v1_clients',
			    Key:{
					'client_id' : {S: client_id}
			    },
			    UpdateExpression: "set brand = :b, device_id = :did, geo_cc = :gcc, geo_city = :gc, geo_region = :gr, geo_zip = :gz, ip_address = :ipa, version = :v, " + req.query.consent_key + " = :ck",
			    ExpressionAttributeValues:{
			        ':b'     : {S: " "},
			        ':did'   : {S: " "},
			        ':gcc'   : {S: " "},
			        ':gr'    : {S: " "},
			        ':gc'    : {S: " "},
			        ':gz'    : {S: " "},
			        ':ipa'   : {S: " "},
			        ':v'     : {S: " "},
			        ':ck'    : {S: " "}
			    }
			};
		}

		clients = await updateItem(ddb, client);
	} else {
		
		/* User registration */
		if (req.query.consent_value == "true") {

			if (p_id_exists) {
				client = {
					TableName: 'atv_dmp_v1_clients',
					Item: {
						'client_id'  : {S: client_id},
						'profile_id' : {S: profile_id},
						'brand'      : {S: getBrand(ua)},
						'device_id'  : {S: device_id},
						'geo_cc'     : {S: location.geo_cc},
						'geo_region' : {S: location.geo_region},
						'geo_city'   : {S: location.geo_city},
						'geo_zip'   : {S: location.geo_zip},
						'ip_address' : {S: ip_address},
						'version'    : {S: getVersion(ua)},
						'discovery_consent'  : {S: "true"}
					}
				};
			} else {

				client = {
					TableName: 'atv_dmp_v1_clients',
					Item: {
						'client_id'  : {S: client_id},
						//'profile_id' : {S: profile_id},
						'brand'      : {S: getBrand(ua)},
						'device_id'  : {S: device_id},
						'geo_cc'     : {S: location.geo_cc},
						'geo_region' : {S: location.geo_region},
						'geo_city'   : {S: location.geo_city},
						'geo_zip'   : {S: location.geo_zip},
						'ip_address' : {S: ip_address},
						'version'    : {S: getVersion(ua)},
						'discovery_consent'  : {S: "true"}
					}
				};
			}
			
			clients = await (putItem(ddb, client));
		} 
	}

	if (req.query.consent_value == "true") {
		/* Audience */
		var ts = Date.now();
		client = {
			TableName: 'atv_dmp_v1_audience',
			Item: {
				'client_id'   : {S: client_id},
				'ts'          : {S: ts.toString()},
				'brand'       : {S: getBrand(ua)},
				'device_id'   : {S: device_id},
				'geo_cc'      : {S: location.geo_cc},
				'geo_region'  : {S: location.geo_region},
				'geo_city'    : {S: location.geo_city},
				'geo_zip'     : {S: location.geo_zip},
				'ip_address'  : {S: ip_address},
				'platform'    : {S: req.query.platform},
				'publisher_id': {S: req.query.publisher_id},
				/* 'service_id' : {S: req.query.service_id}, */
				'ua'          : {S: ua},
				'version'     : {S: getVersion(ua)}
			}
		};

		var audience = await putItem(ddb, client);

		if (clients.succeed === true && audience.succeed === true) {
			var data = {
				client_id:    client_id,
				geo_cc:       location.geo_cc,
				geo_region:   location.geo_region,
				geo_city:     location.geo_city,
				geo_zip:      location.geo_zip,
				geo_province: location.geo_province
			};

			res.status(200);
			if (output_type === "json") {
				res.send(JSON.stringify(data));
			} else {
				res.setHeader('Content-Type', 'application/javascript');
				res.send(
					callback + '(' + JSON.stringify(data) + ')'
				);
			}
			return;
		} else {
			res.send(errorObj(1003, "An unknown error has occurred."));
			return;
		}
	} else {
		var data = {};

		if (output_type === "json") {
			res.send(JSON.stringify(data));
		} else {
			res.setHeader('Content-Type', 'application/javascript');
			res.send(
				callback + '(' + JSON.stringify(data) + ')'
			);
		}
		return;
	}
};

async function getUuid(name) {
	/* return new promise */ 
	return new Promise(function(resolve, reject) {
        /* do async job */
        UUID.v5({
	    	namespace: UUID.namespace.cstm, /* url */
	    	name:      name
		}, function (err, result) {
	      	if (err === null) {
	        	resolve(result);
	      	} else {
	        	reject(result);
	      	}
	    });
    })  
};

async function getItem(ddb, item) {
	/* return new promise */ 
	return new Promise(function(resolve, reject) {
        try {
        	ddb.query(item, function(err, data) {
			    if (err) {
			        reject({succeed: false});
			    } else {
			    	resolve({data: data.Items});
			    }
			});
        } catch (e) {
			reject({succeed: false});
		}
    })
};

async function putItem(ddb, item) {
	/* return new promise */ 
	return new Promise(function(resolve, reject) {
        try {
        	ddb.putItem(item, function(err, data) {
			    if (err)
			        reject({succeed: false});
			    else
			    	resolve({succeed: true});
			});
        } catch (e) {
			reject({succeed: false});
		}
    })
};

async function updateItem(ddb, item) {
	/* return new promise */ 
	return new Promise(function(resolve, reject) {
        try {
        	ddb.updateItem(item, function(err, data) {
			    if (err)
			        reject({succeed: false});
			    else
			    	resolve({succeed: true});
			});
        } catch (e) {
			reject({succeed: false});
		}
    })
};

async function doRequest(url) {
    /* return new promise */ 
    return new Promise(function(resolve, reject) {
        /* do async job */
        request(url, function(error, response, body) { 
        	if (!error && response.statusCode == 200) {
	        	resolve(response.body);
	      	} else {
	        	reject(error);
	      	}
        })
    });
};

async function getGeo(ip_address, access_key) {
    var url = "http://3.121.209.57/geo.php?ip="+ip_address+"&access_key="+access_key;
    /* return new promise */ 
    return new Promise(function(resolve, reject) {
        /* do async job */
        request(url, function(error, response, body) { 
        	if (!error && response.statusCode == 200) {
	        	resolve(response.body);
	      	} else {
	        	reject(error);
	      	}
        })
    });
};

function getVersion(ua) {
	ua = ua.toLowerCase();
	var version = "unknown";

	if (ua.indexOf("hbbtv/1.1.1") !== -1) {
        version = "HbbTV/1.1";
    } else if (ua.indexOf("hbbtv/1.2.1") !== -1) {
        version = "HbbTV/1.5";
    } else if (ua.indexOf("hbbtv/1.3.1") !== -1) {
        version = "HbbTV/2.0";
    } else if (ua.indexOf("hbbtv/1.4.1") !== -1) {
        version = "HbbTV/2.0.1";
    } else if (ua.indexOf("hbbtv/1.5.1") !== -1) {
        version = "HbbTV/2.0.2";
    } 

    return version;
};

function getBrand(ua) {
	ua = ua.toLowerCase();
	var brand = "unknown";
	var brands = ["samsung", "philips", "sony", "lge", "panasonic", "toshiba", "vestel", "jvc", "firetv", "hisense", "sharp", "mstar", "tcl", "tpvision", "tesla", "grundig", "medion", "hitachi", "hyundai", "manta", "orion", "nec"];

	brands.forEach(function(b) {
		if (ua.indexOf(b) !== -1) {
	        brand = b;
	        return brand;
	    }
	});

    return brand;
};

function checkRequired(param) {
	if (typeof param === "undefined" || param === null || param === "") {
		return true;
	}
	return false;
};

function checkOptional(param) {
	if (typeof param !== "undefined" && param !== null && param !== "") {
		return true;
	}
	return false;
};

function errorObj(error_code, message) {
	return {
    	error_code: error_code,
      	message:    message
	};
};

function jsonp(res, callback, data) {
	res.setHeader('Content-Type', 'application/javascript');
	res.send(
		callback + '(' + JSON.stringify(data) + ')'
	);
	return;
};
/* Load the AWS SDK for Node.js */
const AWS        = require('aws-sdk');
const request    = require('request');
const UUID       = require('/opt/bitnami/apache2/htdocs/atv-dmp/uuid.js');
const crypto     = require('crypto');
const placements = require('/opt/bitnami/apache2/htdocs/atv-dmp/config.json');

AWS.config.update({
    accessKeyId: 	 'AKIAJ2HE4GU3YAO472CA',
    secretAccessKey: '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',
    region: 		 'eu-central-1'
});

const s3         = new AWS.S3();
const dynamodb   = new AWS.DynamoDB();
const lambda     = new AWS.Lambda();
const docClient  = new AWS.DynamoDB.DocumentClient();

const cron    = require("node-cron");
const express = require("express");
const cluster = require('cluster');
const fs      = require("fs");
const os      = require('os');

app = express();

var ddb = new AWS.DynamoDB({
    apiVersion: '2012-08-10'
});

var imageHex = "\x42\x4d\x3c\x00\x00\x00\x00\x00\x00\x00\x36\x00\x00\x00\x28\x00"+ 
"\x00\x00\x01\x00\x00\x00\x01\x00\x00\x00\x01\x00\x18\x00\x00\x00"+ 
"\x00\x00\x06\x00\x00\x00\x27\x00\x00\x00\x27\x00\x00\x00\x00\x00"+
"\x00\x00\x00\x00\x00\x00\xff\xff\xff\xff\x00\x00"; 

// Code to run if we're in the master process
if (cluster.isMaster) {
    var cpuCount = os.cpus().length;

    for (var i = 0; i < cpuCount; i += 1) {
        cluster.fork();
    }

    cluster.on('exit', function (worker) {
        console.log({
        	worker_id: worker.id,
        	event: "exit"
        });

        cluster.fork();
    });
// Code to run if we're in a worker process
} else {
	var cpuCount = os.cpus().length;

    app.get('/log-impression', function (req, res) {    	
    	logImpression(req, res);
    });

    app.get('/log-click', function (req, res) {    	
    	logClick(req, res);
    });

	app.get('/log-conversion', function (req, res) {    	
    	logConversion(req, res);
    });

    var server = app.listen(3004, function () {
	  	var host = server.address().address;
	    var port = server.address().port;
	    console.log("[castoola-atv-dmp] log-app listening at http://%s:%s", host, port);
	});
}

async function logImpression(req, res) { 
	/* Default response settings */
    res.setHeader('Content-Type', 'application/json');
	res.status(400);

	var publisher_id, placement_id, campaign_id, creative_id, client_id, output_type, geo_cc, geo_region, geo_city, geo_zip = null;

	if (req.query.publisher_id) {
		publisher_id = req.query.publisher_id;
	} else {
		res.send(errorObj(5001, "Publisher ID is invalid or missing."));
		return;
	}

	if (req.query.placement_id) {
		placement_id = req.query.placement_id;
	} else {
		res.send(errorObj(5002, "Placement ID is invalid or missing."));
		return;
	}

	if (req.query.campaign_id) {
		campaign_id = req.query.campaign_id;
	} else {
		res.send(errorObj(5003, "Campaign ID is invalid or missing."));
		return;
	}

	if (req.query.creative_id) {
		creative_id = req.query.creative_id;
	} else {
		res.send(errorObj(5004, "Creative ID is invalid or missing."));
		return;
	}
	
	if (req.query.client_id) {
		client_id = req.query.client_id;

		if (!UUID.check(req.query.client_id)) {
			res.send(errorObj(5001, "UUID is invalid or missing."));
			return;
		}
	} else {
		res.send(errorObj(5005, "Client ID is invalid or missing."));
		return;	
	}

	if (typeof req.query.output_type !== "undefined") {
		if (req.query.output_type == "px" || req.query.output_type == "json") {
			output_type = req.query.output_type;
		} else {
			res.send(errorObj(5006, "Output type is invalid (json or px)."));
			return;
		}
	} else {
		output_type = "px";
	}

	if (req.query.impression_tracker) {
		impression_tracker = req.query.impression_tracker;
	}

	if (req.query.geo_cc) {
		geo_cc = req.query.geo_cc;
	} else {
		geo_cc = " ";
	}

	if (req.query.geo_region) {
		geo_region = req.query.geo_region;
	} else {
		geo_region = " ";
	}

	if (req.query.geo_city) {
		geo_city = req.query.geo_city;
	} else {
		geo_city = " ";
	}

	if (req.query.geo_zip) {
		geo_zip = req.query.geo_zip;
	} else {
		geo_zip = " ";
	}

	var ts = Date.now();
	var params = {
		TableName: 'atv_dmp_v1_advertising',
		Item: {
			'client_id': 	{S: client_id},
			'ts':    		{S: ts.toString()},
			'action': 	 	{S: "impression"},
			'publisher_id': {S: publisher_id},
			'placement_id': {S: placement_id},
			'campaign_id': 	{S: campaign_id},
			'creative_id': 	{S: creative_id},
			'geo_cc': 		{S: geo_cc},
			'geo_region': 	{S: geo_region},
			'geo_city': 	{S: geo_city},
			'geo_zip': 	 	{S: geo_zip}
		}
	};

	var imp = await putItem(ddb, params);
	if (imp.succeed === true) {
		res.status(200);
		if (output_type === "px")
			return px(res);
		else {
			res.send({success: "ok"});
			return;
		}
	} else {
		res.send(errorObj(5007, "An unknown error has occurred."));
		return;
	}
};

async function logClick(req, res) {
	/* Default response settings */
    res.setHeader('Content-Type', 'application/json');
	res.status(400);

	var publisher_id, placement_id, campaign_id, creative_id, client_id, geo_cc, geo_region, geo_city, geo_zip = null;

	if (req.query.publisher_id) {
		publisher_id = req.query.publisher_id;
	} else {
		res.send(errorObj(6001, "Publisher ID is invalid or missing."));
		return;
	}

	if (req.query.placement_id) {
		placement_id = req.query.placement_id;
	} else {
		res.send(errorObj(6002, "Placement ID is invalid or missing."));
		return;
	}

	if (req.query.campaign_id) {
		campaign_id = req.query.campaign_id;
	} else {
		res.send(errorObj(6003, "Campaign ID is invalid or missing."));
		return;
	}

	if (req.query.creative_id) {
		creative_id = req.query.creative_id;
	} else {
		res.send(errorObj(6004, "Creative ID is invalid or missing."));
		return;
	}
	
	if (req.query.client_id) {
		client_id = req.query.client_id;

		if (!UUID.check(req.query.client_id)) {
			res.send(errorObj(6001, "UUID is invalid or missing."));
			return;
		}
	} else {
		res.send(errorObj(6005, "Client ID is invalid or missing."));
		return;	
	}

	if (typeof req.query.output_type !== "undefined") {
		if (req.query.output_type == "px" || req.query.output_type == "json") {
			output_type = req.query.output_type;
		} else {
			res.send(errorObj(6006, "Output type is invalid (json or px)."));
			return;
		}
	} else {
		output_type = "px";
	}

	if (req.query.geo_cc) {
		geo_cc = req.query.geo_cc;
	} else {
		geo_cc = " ";
	}

	if (req.query.geo_region) {
		geo_region = req.query.geo_region;
	} else {
		geo_region = " ";
	}

	if (req.query.geo_city) {
		geo_city = req.query.geo_city;
	} else {
		geo_city = " ";
	}

	if (req.query.geo_zip) {
		geo_zip = req.query.geo_zip;
	} else {
		geo_zip = " ";
	}

	var ts = Date.now();
	var params = {
		TableName: 'atv_dmp_v1_advertising',
		Item: {
			'client_id': 	{S: client_id},
			'ts':    		{S: ts.toString()},
			'action': 	 	{S: "click"},
			'publisher_id': {S: publisher_id},
			'placement_id': {S: placement_id},
			'campaign_id': 	{S: campaign_id},
			'creative_id': 	{S: creative_id},
			'geo_cc': 		{S: geo_cc},
			'geo_region': 	{S: geo_region},
			'geo_city': 	{S: geo_city},
			'geo_zip': 	 	{S: geo_zip}
		}
	};

	var clk = await putItem(ddb, params);
	if (clk.succeed === true) {
		res.status(200);
		if (output_type === "px")
			return px(res);
		else {
			res.send({success: "ok"});
			return;
		}
	} else {
		res.send(errorObj(6007, "An unknown error has occurred."));
		return;
	}
};

async function logConversion(req, res) {
	/* Default response settings */
	res.setHeader('Content-Type', 'application/json');
	res.status(400);

	var publisher_id, placement_id, campaign_id, creative_id, client_id, geo_cc, geo_region, geo_city, geo_zip = null;

	if (req.query.publisher_id) {
		publisher_id = req.query.publisher_id;
	} else {
		res.send(errorObj(7001, "Publisher ID is invalid or missing."));
		return;
	}

	if (req.query.placement_id) {
		placement_id = req.query.placement_id;
	} else {
		res.send(errorObj(7002, "Placement ID is invalid or missing."));
		return;
	}

	if (req.query.campaign_id) {
		campaign_id = req.query.campaign_id;
	} else {
		res.send(errorObj(7003, "Campaign ID is invalid or missing."));
		return;
	}

	if (req.query.creative_id) {
		creative_id = req.query.creative_id;
	} else {
		res.send(errorObj(7004, "Creative ID is invalid or missing."));
		return;
	}
	
	if (req.query.client_id) {
		client_id = req.query.client_id;

		if (!UUID.check(req.query.client_id)) {
			res.send(errorObj(7001, "UUID is invalid or missing."));
			return;
		}
	} else {
		res.send(errorObj(7005, "Client ID is invalid or missing."));
		return;	
	}

	if (typeof req.query.output_type !== "undefined") {
		if (req.query.output_type == "px" || req.query.output_type == "json") {
			output_type = req.query.output_type;
		} else {
			res.send(errorObj(7006, "Output type is invalid (json or px)."));
			return;
		}
	} else {
		output_type = "px";
	}

	if (req.query.geo_cc) {
		geo_cc = req.query.geo_cc;
	} else {
		geo_cc = " ";
	}

	if (req.query.geo_region) {
		geo_region = req.query.geo_region;
	} else {
		geo_region = " ";
	}

	if (req.query.geo_city) {
		geo_city = req.query.geo_city;
	} else {
		geo_city = " ";
	}

	if (req.query.geo_zip) {
		geo_zip = req.query.geo_zip;
	} else {
		geo_zip = " ";
	}

	var ts = Date.now();
	var params = {
		TableName: 'atv_dmp_v1_advertising',
		Item: {
			'client_id': 	{S: client_id},
			'ts':    		{S: ts.toString()},
			'action': 	 	{S: "conversion"},
			'publisher_id': {S: publisher_id},
			'placement_id': {S: placement_id},
			'campaign_id': 	{S: campaign_id},
			'creative_id': 	{S: creative_id},
			'geo_cc': 		{S: geo_cc},
			'geo_region': 	{S: geo_region},
			'geo_city': 	{S: geo_city},
			'geo_zip': 	 	{S: geo_zip}
		}
	};

	var cnv = await putItem(ddb, params);
	if (cnv.succeed === true) {
		res.status(200);
		if (output_type === "px")
			return px(res);
		else {
			res.send({success: "ok"});
			return;
		}
	} else {
		res.send(errorObj(7007, "An unknown error has occurred."));
		return;
	}
};

async function putItem(ddb, item) {
	/* return new promise */ 
	return new Promise(function(resolve, reject) {
        try {
        	ddb.putItem(item, function(err, data) {
			    if (err)
			        reject({succeed: false});
			    else
			    	resolve({succeed: true});
			});
        } catch (e) {
			reject({succeed: false});
		}
    })
};

function errorObj(error_code, message) {
	return {
    	error_code: error_code,
      	message:    message
	};
};

function px(res) {
	res.status(200);
	res.set('Access-Control-Allow-Origin', '*');
	res.set('Content-Type', 'image/gif');
	res.set('isBase64Encoded', true);
	res.send(imageHex);
	return;
};
/* Load the AWS SDK for Node.js */
const AWS        = require('aws-sdk');
const request    = require('request');
const UUID       = require('/opt/bitnami/apache2/htdocs/atv-dmp/uuid.js');
const crypto     = require('crypto');
const placements = require('/opt/bitnami/apache2/htdocs/atv-dmp/config.json');

AWS.config.update({
    accessKeyId: 	 'AKIAJ2HE4GU3YAO472CA',
    secretAccessKey: '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',
    region: 		 'eu-central-1'
});

const s3         = new AWS.S3();
const dynamodb   = new AWS.DynamoDB();
const lambda     = new AWS.Lambda();
const docClient  = new AWS.DynamoDB.DocumentClient();

const cron    = require("node-cron");
const express = require("express");
const cluster = require('cluster');
const fs      = require("fs");
const os      = require('os');

app = express();

var ddb = new AWS.DynamoDB({
    apiVersion: '2012-08-10'
});

// Code to run if we're in the master process
if (cluster.isMaster) {
    var cpuCount = os.cpus().length;

    for (var i = 0; i < cpuCount; i += 1) {
        cluster.fork();
    }

    cluster.on('exit', function (worker) {
        console.log({
        	worker_id: worker.id,
        	event: "exit"
        });

        cluster.fork();
    });
// Code to run if we're in a worker process
} else {
	var cpuCount = os.cpus().length;

    app.get('/get-ad', function (req, res) {    	
    	getAd(req, res);
    });

    var server = app.listen(3003, function () {
	  	var host = server.address().address;
	    var port = server.address().port;
	    console.log("[castoola-atv-dmp] ad-app listening at http://%s:%s", host, port);
	});
}

async function getAd(req, res) {
	/* Default response settings */
    res.setHeader('Content-Type', 'application/json');
	res.status(400);

	var qs, r = {};
	var config_data, profile_cookie, x_forwarded_for, host = null;
	var plc_id, publisher_id, type, callback, client_id, profile_id, width, height = null;
	var geo_cc, geo_region, geo_city = "";
	var c_ad_type, c_adserver_domain, c_adserver_type, c_adserver_endpoint, c_adserver_zoneid, c_dmp_type, c_dmp_hostname, c_dmp_port;

	x_forwarded_for = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
	x_forwarded_for = x_forwarded_for.replace(/^.*:/, '');
	// host 		= event.headers.Host;
	host = "xcluzn0awl.execute-api.eu-central-1.amazonaws.com";
	
	/* 
		Get atributes
	 */
	if (isUndefinedNull(req.query.placement_id)) {
		res.send(errorObj(4001, "Placement ID is invalid or missing."));
		return;
	} else {
		plc_id = req.query.placement_id;
	
		var pub_id_tmp = plc_id.split("_");
		if (!isUndefinedNull(pub_id_tmp[0])) {
			publisher_id = pub_id_tmp[0];
		} else {
			res.send(errorObj(4004, "The configuration doesn't exist for this placement ID."));
			return;
		}
	}

	if (isUndefinedNull(req.query.response_type)) {
		res.send(errorObj(4002, "Response type is invalid or missing."));
		return;
	} else {
		type = req.query.response_type;
	}

	if (isUndefinedNull(req.query.client_id)) {
		res.send(errorObj(4003, "Client ID is invalid or missing."));
		return;
	} else {
		client_id = req.query.client_id;
	}

	if (!isUndefinedNull(req.query.callback)) {
		callback = req.query.callback;
	}

	if (!isUndefinedNull(req.query.profile_id)) {
		profile_id = req.query.profile_id;
	}

	if (!isUndefinedNull(req.query.width)) {
		width = req.query.width;
	}

	if (!isUndefinedNull(req.query.height)) {
		height = req.query.height;
	}

	if (!isUndefinedNull(req.query.geo_cc)) {
		geo_cc = req.query.geo_cc;
	}

	if (!isUndefinedNull(req.query.geo_region)) {
		geo_region = req.query.geo_region;
	}

	if (!isUndefinedNull(req.query.geo_city)) {
		geo_city = req.query.geo_city;
	}

	/*
		Check config and get variables
	*/ 
	if (placements) {
		placements.forEach(function(element) {
			if (element.plc_id != null && element.plc_id == plc_id) {
				config_data = element;
			}
		});
	}
	console.log("config_data =", config_data);

	if (config_data == null) {
		res.send(errorObj(4004, "The configuration doesn't exist for this placement ID."));
		return;
	}
	if (typeof config_data.ad_type != 'undefined' && config_data.ad_type) {
		c_ad_type = config_data.ad_type;
	}
	if (typeof config_data.adserver_domain != 'undefined' && config_data.adserver_domain) {
		c_adserver_domain = config_data.adserver_domain;
	}
	if (typeof config_data.adserver_type != 'undefined' && config_data.adserver_type) {
		c_adserver_type = config_data.adserver_type;
	}
	if (typeof config_data.adserver_endpoint != 'undefined' && config_data.adserver_endpoint) {
		c_adserver_endpoint = config_data.adserver_endpoint;
	}
	if (typeof config_data.adserver_zoneid != 'undefined' && config_data.adserver_zoneid) {
		c_adserver_zoneid = config_data.adserver_zoneid;
	}
	if (typeof config_data.dmp_type != 'undefined' && config_data.dmp_type) {
		c_dmp_type = config_data.dmp_type;
	}
	if (typeof config_data.dmp_hostname != 'undefined' && config_data.dmp_hostname) {
		c_dmp_hostname = config_data.dmp_hostname;
	}
	if (typeof config_data.c_dmp_port != 'undefined' && config_data.c_dmp_port) {
		c_dmp_port = config_data.c_dmp_port;
	}

	/*
		Request on ad server
	 */
	if (c_adserver_domain && c_adserver_endpoint && c_adserver_zoneid) {
		if (c_adserver_type == "atv-revive") {
			var url = "http://";
			var geo = "";

			url += c_adserver_domain;
			url += c_adserver_endpoint;
			url += "?zoneid=" + c_adserver_zoneid;

			if (geo_cc) {
				geo += "&geo_cc=" + geo_cc;
			}
			if (geo_region) {
				geo += "&geo_region=" + geo_region;
			}
			if (geo_city) {
				geo += "&geo_city=" + geo_city;
			}

			console.log("url =", url);
			console.log("geo =", geo);

			var res_adserver = await doRequest(url + geo);

			if (res_adserver.statusCode == 200) {
				/* Get data from adserver */ 
				var ret_ad 	= getDataFormAdServer(res_adserver, c_adserver_type);

				if (ret_ad.ad_served == true) {
					r.ad_served 		= ret_ad.ad_served;
					r.ad_type 			= c_ad_type;
					r.adserver_domain 	= c_adserver_domain;
					r.adserver_type 	= c_adserver_type;
					r.creative 			= ret_ad.creative;
					
					var plc_id_tmp = plc_id.split("-");
					
					var im_tr = "https://"+host+"/"+"v1/ads/log-impression?publisher_id=" + plc_id_tmp[0] + "&placement_id="+plc_id+"&campaign_id=" + r.creative.campaign_id + "&creative_id=" + r.creative.creative_id + "&client_id="+client_id;
					if (!isUndefinedNull(profile_id)) {
						im_tr += "&profile_id="+profile_id;
					}
					if (geo_cc) {
						im_tr += "&geo_cc="+geo_cc;
					}
					if (geo_region) {
						im_tr += "&geo_region="+geo_region;
					}
					if (geo_city) {
						im_tr += "&geo_city="+geo_city;
					}

					r.creative.impression_trackers = new Array(im_tr);

					var cl_tr = "https://"+host+"/"+"v1/ads/log-click?publisher_id=" + plc_id_tmp[0] + "&placement_id="+plc_id+"&campaign_id=" + r.creative.campaign_id + "&creative_id=" + r.creative.creative_id + "&client_id="+client_id;
					if (!isUndefinedNull(profile_id)) {
						cl_tr += "&profile_id="+profile_id;
					}
					if (geo_cc) {
						cl_tr += "&geo_cc="+geo_cc;
					}
					if (geo_region) {
						cl_tr += "&geo_region="+geo_region;
					}
					if (geo_city) {
						cl_tr += "&geo_city="+geo_city;
					}
					if (!isUndefinedNull(r.creative.click_trackers)) {
						cl_tr += "&click_tracker="+encodeURIComponent(r.creative.click_trackers);
					}

					r.creative.click_trackers 	= new Array(cl_tr);
					r.creative.creative_url 	= r.creative.creative_url + geo;
				} else {
					r.ad_served = false;
				} 

			    if (type == "json") {
			    	res.status(200);

				    if (!isUndefinedNull(callback)) {
				    	res.setHeader('Content-Type', 'application/javascript');
						res.send(
							callback + '(' + JSON.stringify(r) + ')'
						);
				    } else {
				    	res.send(JSON.stringify(r));
						return;
				    }
			    }
			    else {
			    	res.send(errorObj(4002, "Response type is invalid or missing."));
					return;
			    }
			} else {
				res.send(errorObj(4006, "An unknown error has occurred."));
				return;
			}

			res.send(r);
			return;
		} else {
			res.send(errorObj(4005, "Wrong ad server configuration."));
			return;
		}
	} else {
		res.send(errorObj(4005, "Wrong ad server configuration."));
		return;
	}

	res.status(200);
	res.send({
		x_forwarded_for: x_forwarded_for,
		host:            req.headers.host,
		os:              os.hostname(),
		success:         "ok"
	});
	return;
};

/*
	UUID
 */
async function getUuid(name) {
	/* return new promise */ 
	return new Promise(function(resolve, reject) {
        /* do async job */
        UUID.v5({
	    	namespace: UUID.namespace.cstm, /* url */
	    	name:      name
		}, function (err, result) {
	      	if (err === null) {
	        	resolve(result);
	      	} else {
	        	reject(result);
	      	}
	    });
    })  
};

/*
	S3 bucket functions
 */
async function getItem(ddb, item) {
	/* return new promise */ 
	return new Promise(function(resolve, reject) {
        try {
        	ddb.query(item, function(err, data) {
			    if (err) {
			        reject({succeed: false});
			    } else {
			    	resolve({data: data.Items});
			    }
			});
        } catch (e) {
			reject({succeed: false});
		}
    })
};

async function putItem(ddb, item) {
	/* return new promise */ 
	return new Promise(function(resolve, reject) {
        try {
        	ddb.putItem(item, function(err, data) {
			    if (err)
			        reject({succeed: false});
			    else
			    	resolve({succeed: true});
			});
        } catch (e) {
			reject({succeed: false});
		}
    })
};

async function updateItem(ddb, item) {
	/* return new promise */ 
	return new Promise(function(resolve, reject) {
        try {
        	ddb.updateItem(item, function(err, data) {
			    if (err)
			        reject({succeed: false});
			    else
			    	resolve({succeed: true});
			});
        } catch (e) {
			reject({succeed: false});
		}
    })
};

/*
	Default doRequest
 */
async function doRequest(url) {
    /* return new promise */ 
    return new Promise(function(resolve, reject) {
        /* do async job */
        request(url, function(error, response, body) { 
        	if (!error && response.statusCode == 200) {
	        	resolve(response);
	      	} else {
	        	reject(error);
	      	}
        })
    });
};

/*
	Geolocation do request (microservice)
 */
async function getGeo(ip_address, access_key) {
    var url = "http://3.121.209.57/geo.php?ip="+ip_address+"&access_key="+access_key;
    /* return new promise */ 
    return new Promise(function(resolve, reject) {
        /* do async job */
        request(url, function(error, response, body) { 
        	if (!error && response.statusCode == 200) {
	        	resolve(response.body);
	      	} else {
	        	reject(error);
	      	}
        })
    });
};

function getDataFormAdServer(data, adserver_type) {
	var ret = {};
	ret.creative = {};
	if (adserver_type == "atv-revive") {
		var json = JSON.parse(data.body);
		if (json.ad_served == true) {
			ret.ad_served = true;
			ret.creative.campaign_id 			= json.campaign_id;
			ret.creative.price  				= parseFloat(json.price);
			ret.creative.creative_id   			= json.creative_id;
			ret.creative.creative_url    		= json.imageUrl;
			ret.creative.width    				= json.width;
			ret.creative.height    				= json.height;
			ret.creative.impression_trackers	= new Array(json.impressionUrl);
			ret.creative.click_trackers			= new Array(json.clickUrl);
			ret.creative.conversion_trackers	= new Array(json.conversionUrl);
			// ret.creative.click_url			= json.clickUrl;
			// ret.creative.impression_url		= json.impressionUrl;
		} else {
			ret.creative.ad_served = false;
		}
	}

	return ret;
}

/*
	Other functions
 */
function getVersion(ua) {
	ua = ua.toLowerCase();
	var version = "unknown";

	if (ua.indexOf("hbbtv/1.1.1") !== -1) {
        version = "HbbTV/1.1";
    } else if (ua.indexOf("hbbtv/1.2.1") !== -1) {
        version = "HbbTV/1.5";
    } else if (ua.indexOf("hbbtv/1.3.1") !== -1) {
        version = "HbbTV/2.0";
    } else if (ua.indexOf("hbbtv/1.4.1") !== -1) {
        version = "HbbTV/2.0.1";
    } else if (ua.indexOf("hbbtv/1.5.1") !== -1) {
        version = "HbbTV/2.0.2";
    } 

    return version;
};

function getBrand(ua) {
	ua = ua.toLowerCase();
	var brand = "unknown";
	var brands = ["samsung", "philips", "sony", "lge", "panasonic", "toshiba", "vestel", "jvc", "firetv", "hisense", "sharp", "mstar", "tcl", "tpvision", "tesla", "grundig", "medion", "hitachi", "hyundai", "manta", "orion", "nec"];

	brands.forEach(function(b) {
		if (ua.indexOf(b) !== -1) {
	        brand = b;
	        return brand;
	    }
	});

    return brand;
};

function isUndefinedNull(param) {
	if (typeof param === "undefined" || param === null || param === "") {
		return true;
	}
	return false;
};

function errorObj(error_code, message) {
	return {
    	error_code: error_code,
      	message:    message
	};
};

function json(res, callback, data) {

}

function jsonp(res, callback, data) {
	res.setHeader('Content-Type', 'application/javascript');
	res.send(
		callback + '(' + JSON.stringify(data) + ')'
	);
	return;
};
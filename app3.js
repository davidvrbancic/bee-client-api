/* Load the AWS SDK for Node.js */
const AWS        = require('aws-sdk');
const request    = require('request');
const UUID       = require('/opt/bitnami/apache2/htdocs/bee-client-api/uuid.js');
const crypto     = require('crypto');
const placements = require('/opt/bitnami/apache2/htdocs/bee-client-api/config.json');

AWS.config.update({
    accessKeyId: 	 'AKIAJ2HE4GU3YAO472CA',
    secretAccessKey: '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',
    region: 		 'eu-central-1'
});

const s3         = new AWS.S3();
const dynamodb   = new AWS.DynamoDB();
const lambda     = new AWS.Lambda();
const docClient  = new AWS.DynamoDB.DocumentClient();

const http 	  = require('http');
const https   = require('https');

const cron    = require("node-cron");
const express = require("express");
const cluster = require('cluster');
const fs      = require("fs");
const os      = require('os');

// Certificate
const privateKey 	= fs.readFileSync('/etc/letsencrypt/live/bee-client-api.castoola.tv/privkey.pem', 'utf8');
const certificate 	= fs.readFileSync('/etc/letsencrypt/live/bee-client-api.castoola.tv/cert.pem', 'utf8');
const ca 			= fs.readFileSync('/etc/letsencrypt/live/bee-client-api.castoola.tv/chain.pem', 'utf8');

const credentials = {
	key: privateKey,
	cert: certificate,
	ca: ca
};

app = express();

var ddb = new AWS.DynamoDB({
    apiVersion: '2012-08-10'
});

var imageHex = "\x42\x4d\x3c\x00\x00\x00\x00\x00\x00\x00\x36\x00\x00\x00\x28\x00"+ 
"\x00\x00\x01\x00\x00\x00\x01\x00\x00\x00\x01\x00\x18\x00\x00\x00"+ 
"\x00\x00\x06\x00\x00\x00\x27\x00\x00\x00\x27\x00\x00\x00\x00\x00"+
"\x00\x00\x00\x00\x00\x00\xff\xff\xff\xff\x00\x00"; 

// Code to run if we're in the master process
if (cluster.isMaster) {
    var cpuCount = os.cpus().length;

    for (var i = 0; i < cpuCount; i += 1) {
        cluster.fork();
    }

    cluster.on('exit', function (worker) {
        console.log({
        	worker_id: worker.id,
        	event: "exit"
        });

        cluster.fork();
    });
// Code to run if we're in a worker process
} else {
	var cpuCount = os.cpus().length;

    app.get('/', function (req, res) {
		res.sendStatus(200);
        return;
    });

    app.get('/set-watch', function (req, res) {    	
    	setWatch(req, res);
    });

    /*app.get('/get-watch', function (req, res) {    	
    	getWatch(req, res);
	});*/
	
	app.get('/get-watch', function (req, res) {    	
    	getWatchCustom(req, res);
    });

    app.get('/set-consent', function (req, res) {    	
    	setConsent(req, res);
    });
    
    app.get('/get-consent', function (req, res) {    	
    	getConsent(req, res);
    });

    app.get('/get-ad', function (req, res) {    	
    	getAd(req, res);
    });

    app.get('/log-impression', function (req, res) {    	
    	logImpression(req, res);
    });

    app.get('/log-click', function (req, res) {    	
    	logClick(req, res);
	});

	app.get('/log-conversion', function (req, res) {    	
    	logConversion(req, res);
	});

	app.get('/log-ad-event', function (req, res) {    	
    	logAdevent(req, res);
	});
	
	app.get('/log-audience', function (req, res) {    	
    	logAudience(req, res);
    });

    app.get('/get-client-id', function (req, res) {    	
    	deviceLogin(req, res);
    });

	var httpServer 	= http.createServer(app);
	var httpsServer = https.createServer(credentials, app);

	httpServer.listen(80, function () {
		var host = httpServer.address().address;
	  	var port = httpServer.address().port;
	  	console.log("[castoola-atv-dmp] listening at http://%s:%s", host, port);
  	});

	httpsServer.listen(443, function () {
		var host = httpsServer.address().address;
	  	var port = httpsServer.address().port;
	  	console.log("[castoola-atv-dmp] listening at http://%s:%s", host, port);
  	});
}

/**
 * 
 * set-watch
 * get-watch 
 */
async function setWatch(req, res) {
	/* Default response settings */
    res.setHeader('Content-Type', 'application/javascript');
	res.status(400);

	var client_id, callback = "setWatch";

	/* Check params (Required) */
	if (req.query.client_id) {
		client_id = req.query.client_id;

		if (!UUID.check(client_id)) {
			return jsonp(res, callback, 
				errorObj(5202, "Client ID is invalid.")
			);
		}
	} else { 
		return jsonp(res, callback, 
			errorObj(5201, "Client ID is invalid or missing.")
		);
	}

	if (req.query.epg_mediaid) {
		epg_mediaid = req.query.epg_mediaid;
	} else { 
		return jsonp(res, callback, 
			errorObj(5203, "EPG Media ID is invalid or missing.")
		);
	}

	if (req.query.callback) {
		callback = req.query.callback;
	} else { 
		return jsonp(res, callback, 
			errorObj(5204, "Callback is invalid or missing.")
		);
	}

	var ts = Date.now();
	var params = {
		TableName: 'atv_dmp_v1_watch',
		Item: {
			'client_id': {S: client_id},
			'ts' : 		 {S: ts.toString()}
		}
	};

	if (req.query.epg_type) {
		params.Item['epg_type']          = {S: req.query.epg_type};
	}
	if (req.query.epg_mediaid) {
		params.Item['epg_mediaid']       = {S: req.query.epg_mediaid};
	}
	if (req.query.epg_showid) {
		params.Item['epg_showid']        = {S: req.query.epg_showid};
	}
	if (req.query.epg_title) {
		params.Item['epg_title']         = {S: req.query.epg_title};
	}
	if (req.query.epg_genreid) {
		params.Item['epg_genreid']       = {S: req.query.epg_genreid};
	}
	if (req.query.epg_epgshowname) {
		params.Item['epg_epgshowname']   = {S: req.query.epg_epgshowname};
	}
	if (req.query.epg_seriesid) {
		params.Item['epg_seriesid']      = {S: req.query.epg_seriesid};
	}
	if (req.query.epg_epgseriesname) {
		params.Item['epg_epgseriesname'] = {S: req.query.epg_epgseriesname};
	}
	if (req.query.epg_titleid) {
		params.Item['epg_titleid']       = {S: req.query.epg_titleid};
	}
	if (req.query.epg_epgprogname) {
		params.Item['epg_epgprogname']   = {S: req.query.epg_epgprogname};
	}

	var watch = await putItem(ddb, params);
	if (watch.succeed === true) {
		res.status(200);
		return jsonp(res, callback, {
			success: "ok"
		});
	} else {
		return jsonp(res, callback, 
			errorObj(5205, "An unknown error has occurred.")
		);
	}
};

async function getWatch(req, res) {
	/* Default response settings */
    res.setHeader('Content-Type', 'application/javascript');
	res.status(400);

	var client_id, callback = "getWatch";

	/* Check params (Required) */
	if (req.query.client_id) {
		client_id = req.query.client_id;

		if (!UUID.check(client_id)) {
			return jsonp(res, callback, 
				errorObj(5102, "Client ID is invalid.")
			);
		}
	} else { 
		return jsonp(res, callback, 
			errorObj(5101, "Client ID is invalid or missing.")
		);
	}

	if (req.query.callback) {
		callback = req.query.callback;
	} else {
		return jsonp(res, callback, 
			errorObj(5103, "Callback is invalid or missing.")
		);
	}

	var filterExpression = "", expressionAttributeValues = {':c_id': {S: client_id}};
	
	var paramKeys = Object.keys(req.query), i = 0;
	if (paramKeys.length < 1) {
		return jsonp(res, callback, 
			errorObj(5104, "Minimum one parameter required.")
		);
	}
	paramKeys.forEach(function(element) {
		if (element.substr(0, 4) === "epg_") {
			/* filterExpression */
			if(i > 0) {
				filterExpression += " AND ";
			}
			filterExpression += element + " = :" + element;

			/* expressionAttributeValues */
			expressionAttributeValues[':' + element] = {
				S: req.query[element]
			};

			i++;
		}
	});

	var ts = Date.now();
	var params = {
	    TableName : "atv_dmp_v1_watch",
		KeyConditionExpression: '#c_id = :c_id',
		FilterExpression: filterExpression,
		ExpressionAttributeNames:{
        	"#c_id": "client_id"
    	},
	    ExpressionAttributeValues: expressionAttributeValues,
	};

	// console.log("params =", params);

	var watch = await getItem(ddb, params);

	res.status(200);
	if (watch.data.length > 0) {
		return jsonp(res, callback, {
			is_watching: true,
			count:       watch.data.length,
			success:     "ok"
		});
	}

	return jsonp(res, callback, {
		is_watching: false,
		success:     "ok"
	});
};

async function getWatchCustom(req, res) {
	/* Default response settings */
    res.setHeader('Content-Type', 'application/javascript');
	res.status(400);

	var client_id, epg_mediaid, callback = "getWatch";

	/* Check params (Required) */
	if (req.query.client_id) {
		client_id = req.query.client_id;

		if (!UUID.check(client_id)) {
			return jsonp(res, callback, 
				errorObj(5102, "Client ID is invalid.")
			);
		}
	} else { 
		return jsonp(res, callback, 
			errorObj(5101, "Client ID is invalid or missing.")
		);
	}

	if (req.query.callback) {
		callback = req.query.callback;
	} else {
		return jsonp(res, callback, 
			errorObj(5103, "Callback is invalid or missing.")
		);
	}

	var epg_mediaids = [], is_mediaid = false;
	if (req.query.epg_mediaid_1) {
		epg_mediaids.push(req.query.epg_mediaid_1);
		is_mediaid = true;
	}
	if (req.query.epg_mediaid_2) {
		epg_mediaids.push(req.query.epg_mediaid_2);
		is_mediaid = true;
	}
	if (req.query.epg_mediaid_3) {
		epg_mediaids.push(req.query.epg_mediaid_3);
		is_mediaid = true;
	}
	if (req.query.epg_mediaid_4) {
		epg_mediaids.push(req.query.epg_mediaid_4);
		is_mediaid = true;
	}

	if (is_mediaid === false) {
		return jsonp(res, callback, 
			errorObj(5104, "Media id is missing.")
		);
	}

	var c = 0, c_mediaid = 0;
	epg_mediaids.forEach(async function(element) {
		var ts = Date.now();
		var params = {
			TableName : "atv_dmp_v1_watch",
			KeyConditionExpression: '#c_id = :c_id',
			FilterExpression: "epg_mediaid = :epg_mediaid",
			ExpressionAttributeNames:{
				"#c_id": "client_id"
			},
			ExpressionAttributeValues:{
				':c_id': 		{ S: client_id },
				':epg_mediaid': { S: element }
			}
		};

		var watch = await getItem(ddb, params);

		/*console.log("watch =", watch);
		console.log("watch.data.length =", watch.data.length);*/

		if (watch.data.length > 0) {
			c = c + watch.data.length;
		}

		if (c_mediaid === epg_mediaids.length - 1) {
			res.status(200);
			if (c > 0) {
				return jsonp(res, callback, {
					is_watching: true,
					count:       c,
					success:     "ok"
				});
			}

			return jsonp(res, callback, {
				is_watching: false,
				success:     "ok"
			});
		}

		c_mediaid++;
	});	
};

/**
 * 
 * set-consent
 * get-consent 
 */
async function setConsent(req, res) {
	/* Default response settings */
    res.setHeader('Content-Type', 'application/json');
	res.status(400);

	/* init */
	var qs, callback, client, clients;
	var device_id   = "0";
	var profile_id  = "0";
	var output_type = "json";
	var d_id_exists = false;
	var p_id_exists = false;

	
	/* Check params (Required) */
	if (checkRequired(req.query.publisher_id)) {
		res.send(errorObj(1001, "Publisher ID is invalid or missing."));
		return;
	}

	if (checkRequired(req.query.platform)) {
		res.send(errorObj(1002, "Platform is invalid or missing."));
		return;
	}

	if (checkRequired(req.query.consent_key)) {
		res.send(errorObj(1003, "Consent key is invalid or missing."));
		return;
	}

	if (checkRequired(req.query.consent_value)) {
		res.send(errorObj(1005, "Consent value is invalid or missing."));
		return;
	} else {
		if (!(req.query.consent_value == "true" || req.query.consent_value == "false")) {	
			res.send(errorObj(1004, "Consent value is invalid."));
			return;
		}
	}

	/* Check params (Geo) */
	var location = {
		geo_cc:       " ",
		geo_region:   " ",
		geo_city:     " ",
		geo_zip:      " ",
		geo_province: " "
	};

	if (req.query.geo_cc) {
		location.geo_cc = req.query.geo_cc;
	}
	if (req.query.geo_region) {
		location.geo_region = req.query.geo_region;
	}
	if (req.query.geo_city) {
		location.geo_city = req.query.geo_city;
	}
	if (req.query.geo_zip) {
		location.geo_zip = req.query.geo_zip;
	}
	if (req.query.geo_province) {
		location.geo_province = req.query.geo_province;
	}

	/* Check params (Optional) */
	if (checkOptional(req.query.device_id)) {
		d_id_exists = true;
		device_id   = req.query.device_id;
	}

	if (checkOptional(req.query.profile_id)) {
		p_id_exists = true;
		profile_id  = req.query.profile_id;
	}

	if (checkOptional(req.query.profile_id)) {
		p_id_exists = true;
		profile_id  = req.query.profile_id;
	}

	if (checkOptional(req.query.callback)) {
		output_type = "jsonp";
		callback    = req.query.callback;
	}

	var ip         = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
		ip         = ip.replace(/^.*:/, '');
	var ua         = req.headers['user-agent']; 								
	var ip_address = crypto.createHash('md5').update(ip).digest("hex");
	var cid_name   = ip.concat(ua);

	if (d_id_exists === true) cid_name = cid_name.concat(device_id);
	if (p_id_exists === true) cid_name = cid_name.concat(profile_id);

	var client_id = await getUuid(cid_name);

	console.log("["+req.query.service_id+"] [setConsent] client_id =", client_id);

	/* Get location data */
	/*var location;
	try {
		location = await getGeo(ip);    
    	location = JSON.parse(location);
	} catch (e) {
		res.send(errorObj(1006, "Geo data retrieving failed."));
		return;
	}*/

	var params = {
	    TableName : "atv_dmp_v1_clients",
	    ExpressionAttributeValues: {
		    ':c_id': {S: client_id}
		},
		KeyConditionExpression: 'client_id = :c_id'
	};

	var get_client = await getItem(ddb, params);

	if (get_client.data.length > 0) {
		if (req.query.consent_value == "true") {
			client = {
	    		TableName: 'atv_dmp_v1_clients',
			    Key:{
					'client_id' : {S: client_id}
			    },
			    UpdateExpression: "set brand = :b, device_id = :did, geo_cc = :gcc, geo_city = :gc, geo_region = :gr, geo_zip = :gz, geo_province = :gp, ip_address = :ipa, version = :v, " + req.query.consent_key + " = :ck",
			    ExpressionAttributeValues:{
			        ':b'     : {S: getBrand(ua)},
			        ':did'   : {S: device_id},
			        ':gcc'   : {S: location.geo_cc},
			        ':gr'    : {S: location.geo_region},
			        ':gc'    : {S: location.geo_city},
					':gz'    : {S: location.geo_zip},
					':gp'    : {S: location.geo_province},
			        ':ipa'   : {S: ip_address},
			        ':v'     : {S: getVersion(ua)},
			        ':ck'    : {S: req.query.consent_value}
			    }
			};
		} else {
			client = {
	    		TableName: 'atv_dmp_v1_clients',
			    Key:{
					'client_id' : {S: client_id}
			    },
			    UpdateExpression: "set brand = :b, device_id = :did, geo_cc = :gcc, geo_city = :gc, geo_region = :gr, geo_zip = :gz, geo_province = :gp, ip_address = :ipa, version = :v, " + req.query.consent_key + " = :ck",
			    ExpressionAttributeValues:{
			        ':b'     : {S: " "},
			        ':did'   : {S: " "},
			        ':gcc'   : {S: " "},
			        ':gr'    : {S: " "},
			        ':gc'    : {S: " "},
					':gz'    : {S: " "},
					':gp'    : {S: " "},
			        ':ipa'   : {S: " "},
			        ':v'     : {S: " "},
			        ':ck'    : {S: " "}
			    }
			};
		}

		clients = await updateItem(ddb, client);
	} else {
		
		/* User registration */
		if (req.query.consent_value == "true") {

			if (p_id_exists) {
				client = {
					TableName: 'atv_dmp_v1_clients',
					Item: {
						'client_id'        : {S: client_id},
						'profile_id'       : {S: profile_id},
						'brand'            : {S: getBrand(ua)},
						'device_id'        : {S: device_id},
						'geo_cc'           : {S: location.geo_cc},
						'geo_region' 	   : {S: location.geo_region},
						'geo_city'   	   : {S: location.geo_city},
						'geo_zip'    	   : {S: location.geo_zip},
						'geo_province'     : {S: location.geo_province},
						'ip_address' 	   : {S: ip_address},
						'version'    	   : {S: getVersion(ua)},
						'discovery_consent': {S: "true"}
					}
				};
			} else {

				client = {
					TableName: 'atv_dmp_v1_clients',
					Item: {
						'client_id'   	   : {S: client_id},
						/*'profile_id' : {S: profile_id},*/
						'brand'      	   : {S: getBrand(ua)},
						'device_id'  	   : {S: device_id},
						'geo_cc'     	   : {S: location.geo_cc},
						'geo_region' 	   : {S: location.geo_region},
						'geo_city'   	   : {S: location.geo_city},
						'geo_zip'   	   : {S: location.geo_zip},
						'geo_province'     : {S: location.geo_province},
						'ip_address' 	   : {S: ip_address},
						'version'    	   : {S: getVersion(ua)},
						'discovery_consent': {S: "true"}
					}
				};
			}
			
			clients = await (putItem(ddb, client));
		} 
	}

	if (req.query.consent_value == "true") {
		/* Audience */
		var ts = Date.now();
		client = {
			TableName: 'atv_dmp_v1_audience',
			Item: {
				'client_id'   : {S: client_id},
				'ts'          : {S: ts.toString()},
				'brand'       : {S: getBrand(ua)},
				'device_id'   : {S: device_id},
				'geo_cc'      : {S: location.geo_cc},
				'geo_region'  : {S: location.geo_region},
				'geo_city'    : {S: location.geo_city},
				'geo_zip'     : {S: location.geo_zip},
				'geo_province': {S: location.geo_province},
				'ip_address'  : {S: ip_address},
				'platform'    : {S: req.query.platform},
				'publisher_id': {S: req.query.publisher_id},
				/*'service_id' : {S: req.query.service_id},*/
				'ua'          : {S: ua},
				'version'     : {S: getVersion(ua)}
			}
		};

		var audience = await putItem(ddb, client);

		if (clients.succeed === true && audience.succeed === true) {
			var data = {
				client_id:    client_id,
				/*geo_cc:       location.geo_cc,
				geo_region:   location.geo_region,
				geo_city:     location.geo_city,
				geo_zip:      location.geo_zip,
				geo_province: location.geo_province*/
			};

			res.status(200);
			if (output_type === "json") {
				res.send(JSON.stringify(data));
			} else {
				res.setHeader('Content-Type', 'application/javascript');
				res.send(
					callback + '(' + JSON.stringify(data) + ')'
				);
			}
			return;
		} else {
			res.send(errorObj(1007, "An unknown error has occurred."));
			return;
		}
	} else {
		var data = {};

		if (output_type === "json") {
			res.send(JSON.stringify(data));
		} else {
			res.setHeader('Content-Type', 'application/javascript');
			res.send(
				callback + '(' + JSON.stringify(data) + ')'
			);
		}
		return;
	}
};

async function getConsent(req, res) {
	/* Default response settings */
    res.setHeader('Content-Type', 'application/json');
	res.status(400);

	/* init */
	var qs, callback, client, clients;
	var device_id   = "0";
	var profile_id  = "0";
	var output_type = "json";
	var d_id_exists = false;
	var p_id_exists = false;
	var consent     = false;

	var publisher_id, service_id, platform, consent_key, consent_value;

	/* Check params (Required) */
	if (req.query.publisher_id) {
		publisher_id = req.query.publisher_id;
	} else { 
		res.send(errorObj(8001, "Publisher ID is invalid or missing."));
		return;
	}

	if (req.query.service_id) {
		service_id = req.query.service_id;
	} else { 
		res.send(errorObj(8002, "Service ID is invalid or missing."));
		return;
	}

	if (req.query.platform) {
		platform = req.query.platform;
	} else { 
		res.send(errorObj(8003, "Platform is invalid or missing."));
		return;
	}

	if (req.query.consent_key) {
		consent_key = req.query.consent_key;
	} else { 
		res.send(errorObj(8004, "Consent key is invalid or missing."));
		return;
	}

	if (req.query.consent_value) {
		/* consent_value = req.query.consent_value; */

		if (req.query.consent_value !== "true" && req.query.consent_value !== "false") {	
			res.send(errorObj(8006, "Consent value is invalid."));
			return;
		}
	} else { 
		res.send(errorObj(8005, "Consent value is invalid or missing."));
		return;
	}

	if (req.query.device_id) {
		d_id_exists = true;
		device_id   = req.query.device_id;
	}

	if (req.query.profile_id) {
		p_id_exists = true;
		profile_id  = req.query.profile_id;
	}

	if (req.query.callback) {
		output_type = "jsonp";
		callback    = req.query.callback;
	}

	/* Check params (Geo) */
	var location = {
		geo_cc:       " ",
		geo_region:   " ",
		geo_city:     " ",
		geo_zip:      " ",
		geo_province: " "
	};

	if (req.query.geo_cc) {
		location.geo_cc = req.query.geo_cc;
	}
	if (req.query.geo_region) {
		location.geo_region = req.query.geo_region;
	}
	if (req.query.geo_city) {
		location.geo_city = req.query.geo_city; 
	}
	if (req.query.geo_zip) {
		location.geo_zip = req.query.geo_zip;
	}
	if (req.query.geo_province) {
		location.geo_province = req.query.geo_province;
	}

	var ip         = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
		ip         = ip.replace(/^.*:/, '');
	var ua         = req.headers['user-agent']; 								
	var ip_address = crypto.createHash('md5').update(ip).digest("hex");
	var cid_name   = ip.concat(ua);

	if (d_id_exists === true) cid_name = cid_name.concat(device_id);
	if (p_id_exists === true) cid_name = cid_name.concat(profile_id);
	
	var client_id = await getUuid(cid_name);

	/* Get location data */
	/*var location;
	try {
		location = await getGeo(ip);    
    	location = JSON.parse(location);
	} catch (e) {
		res.send(errorObj(8007, "Geo data retrieving failed."));
		return;
	}*/

	console.log("["+req.query.service_id+"] [getConsent] client_id =", client_id);

	var params = {
	    TableName : "atv_dmp_v1_clients",
	    ExpressionAttributeValues: {
		    ':c_id': {S: client_id}
		},
		KeyConditionExpression: 'client_id = :c_id'
	};

	var client_consent = await getItem(ddb, params);
	if (client_consent.data.length > 0) {
		try {
			if (typeof client_consent.data[0][consent_key]["S"] !== "undefined") {
				consent = client_consent.data[0][consent_key]["S"];
			}
		} catch (e) {
			res.send(errorObj(8008, "An unknown error has occurred."));
			return;
		}
	}

	if (consent === "true") consent = true;

	if (consent == true) {
		if (p_id_exists) {
			client = {
	    		TableName: 'atv_dmp_v1_clients',
			    Key:{
					'client_id': {S: client_id}
			    },
			    UpdateExpression: "set brand = :b, device_id = :did, geo_cc = :gcc, geo_city = :gc, geo_region = :gr, geo_zip = :gz, geo_province = :gp, ip_address = :ipa, version = :v",
			    ConditionExpression: 'profile_id = :pid',
			    ExpressionAttributeValues:{
			    	':pid': {S: profile_id},
			        ':b'  : {S: getBrand(ua)},
			        ':did': {S: device_id},
			        ':gcc': {S: location.geo_cc},
			        ':gr' : {S: location.geo_region},
			        ':gc' : {S: location.geo_city},
					':gz' : {S: location.geo_zip},
					':gp' : {S: location.geo_province},
			        ':ipa': {S: ip_address},
			        ':v'  : {S: getVersion(ua)}
			    },
			};
		} else {
			client = {
	    		TableName: 'atv_dmp_v1_clients',
			    Key:{
					'client_id': {S: client_id}
			    },
			    UpdateExpression: "set brand = :b, device_id = :did, geo_cc = :gcc, geo_city = :gc, geo_region = :gr, geo_zip = :gz, geo_province = :gp, ip_address = :ipa, version = :v",
			    ExpressionAttributeValues:{
			        ':b'  : {S: getBrand(ua)},
			        ':did': {S: device_id},
			        ':gcc': {S: location.geo_cc},
			        ':gr' : {S: location.geo_region},
			        ':gc' : {S: location.geo_city},
					':gz' : {S: location.geo_zip},
					':gp' : {S: location.geo_province},
			        ':ipa': {S: ip_address},
			        ':v'  : {S: getVersion(ua)}
			    },
			};
		}

		clients = await (updateItem(ddb, client));
	}	

	if (consent == true) {
		/* Audience */
		var ts = Date.now();
		client = {
			TableName: 'atv_dmp_v1_audience',
			Item: {
				'client_id'   : {S: client_id},
				'ts'          : {S: ts.toString()},
				'brand'       : {S: getBrand(ua)},
				'device_id'   : {S: device_id},
				'geo_cc'      : {S: location.geo_cc},
				'geo_region'  : {S: location.geo_region},
				'geo_city'    : {S: location.geo_city},
				'geo_zip'     : {S: location.geo_zip},
				'geo_province': {S: location.geo_province},
				'ip_address'  : {S: ip_address},
				'platform'    : {S: platform},
				'publisher_id': {S: publisher_id},
				'service_id'  : {S: service_id},
				'ua'          : {S: ua},
				'version'     : {S: getVersion(ua)}
			}
		};

		var audience = await putItem(ddb, client);

		if (clients.succeed === true && audience.succeed === true) {
			var data = {
				client_id: 	  client_id,
				/*geo_cc: 	  location.geo_cc,
				geo_region:   location.geo_region,
				geo_city: 	  location.geo_city,
				geo_zip: 	  location.geo_zip,
				geo_province: location.geo_province*/
			};

			res.status(200);
			if (output_type === "json") {
				res.send(JSON.stringify(data));
			} else {
				res.setHeader('Content-Type', 'application/javascript');
				res.send(
					callback + '(' + JSON.stringify(data) + ')'
				);
			}
			return;
		} else {
			res.send(errorObj(8009, "An unknown error has occurred."));
			return;
		}
	} else {
		var data = {};

		res.status(200);
		if (output_type === "json") {
			res.send(JSON.stringify(data));
		} else {
			res.setHeader('Content-Type', 'application/javascript');
			res.send(
				callback + '(' + JSON.stringify(data) + ')'
			);
		}
		return;
	}
};

/**
 * 
 * get-ad 
 */
async function getAd(req, res) {
	/* Default response settings */
    res.setHeader('Content-Type', 'application/json');
	res.status(400);

	var qs, r = {};
	var config_data, profile_cookie, x_forwarded_for, host = null;
	var plc_id, publisher_id, type, callback, client_id, profile_id, width, height = null;
	var geo_cc, geo_region, geo_city = "", geo_province = "";
	var c_ad_type, c_adserver_domain, c_adserver_type, c_adserver_endpoint, c_adserver_zoneid, c_dmp_type, c_dmp_hostname, c_dmp_port;

	x_forwarded_for = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
	x_forwarded_for = x_forwarded_for.replace(/^.*:/, '');
	host 		    = req.hostname;
	// host = "xcluzn0awl.execute-api.eu-central-1.amazonaws.com";

	/* 
		Get atributes
	 */
	if (checkRequired(req.query.placement_id)) {
		res.send(errorObj(4001, "Placement ID is invalid or missing."));
		return;
	} else {
		plc_id = req.query.placement_id;
	
		var pub_id_tmp = plc_id.split("_");
		if (!checkRequired(pub_id_tmp[0])) {
			publisher_id = pub_id_tmp[0];
		} else {
			res.send(errorObj(4004, "The configuration doesn't exist for this placement ID."));
			return;
		}
	}

	if (checkRequired(req.query.response_type)) {
		res.send(errorObj(4002, "Response type is invalid or missing."));
		return;
	} else {
		type = req.query.response_type;
	}

	if (checkRequired(req.query.client_id)) {
		res.send(errorObj(4003, "Client ID is invalid or missing."));
		return;
	} else {
		client_id = req.query.client_id;
	}

	if (!checkRequired(req.query.callback)) {
		callback = req.query.callback;
	}

	if (!checkRequired(req.query.profile_id)) {
		profile_id = req.query.profile_id;
	}

	if (!checkRequired(req.query.width)) {
		width = req.query.width;
	}

	if (!checkRequired(req.query.height)) {
		height = req.query.height;
	}

	if (!checkRequired(req.query.geo_cc)) {
		geo_cc = req.query.geo_cc;
	}

	if (!checkRequired(req.query.geo_region)) {
		geo_region = req.query.geo_region;
	}

	if (!checkRequired(req.query.geo_city)) {
		geo_city = req.query.geo_city;
	}

	if (!checkRequired(req.query.geo_province)) {
		geo_province = req.query.geo_province;
	}

	/*
		Check config and get variables
	*/ 
	if (placements) {
		placements.forEach(function(element) {
			if (element.plc_id != null && element.plc_id == plc_id) {
				config_data = element;
			}
		});
	}
	// console.log("config_data =", config_data);

	if (config_data == null) {
		res.send(errorObj(4004, "The configuration doesn't exist for this placement ID."));
		return;
	}
	if (typeof config_data.ad_type != 'undefined' && config_data.ad_type) {
		c_ad_type = config_data.ad_type;
	}
	if (typeof config_data.adserver_domain != 'undefined' && config_data.adserver_domain) {
		c_adserver_domain = config_data.adserver_domain;
	}
	if (typeof config_data.adserver_type != 'undefined' && config_data.adserver_type) {
		c_adserver_type = config_data.adserver_type;
	}
	if (typeof config_data.adserver_endpoint != 'undefined' && config_data.adserver_endpoint) {
		c_adserver_endpoint = config_data.adserver_endpoint;
	}
	if (typeof config_data.adserver_zoneid != 'undefined' && config_data.adserver_zoneid) {
		c_adserver_zoneid = config_data.adserver_zoneid;
	}
	if (typeof config_data.dmp_type != 'undefined' && config_data.dmp_type) {
		c_dmp_type = config_data.dmp_type;
	}
	if (typeof config_data.dmp_hostname != 'undefined' && config_data.dmp_hostname) {
		c_dmp_hostname = config_data.dmp_hostname;
	}
	if (typeof config_data.c_dmp_port != 'undefined' && config_data.c_dmp_port) {
		c_dmp_port = config_data.c_dmp_port;
	}

	/*
		Request on ad server
	 */
	if (c_adserver_domain && c_adserver_endpoint && c_adserver_zoneid) {
		if (c_adserver_type == "atv-revive") {
			var url = "http://";
			var geo = "";

			url += c_adserver_domain;
			url += c_adserver_endpoint;
			url += "?zoneid=" + c_adserver_zoneid;

			if (geo_cc) {
				geo += "&geo_cc=" + geo_cc;
			}
			if (geo_region) {
				geo += "&geo_region=" + geo_region;
			}
			if (geo_city) {
				geo += "&geo_city=" + geo_city;
			}
			if (geo_province) {
				geo += "&geo_province=" + geo_province;
			}

			// console.log("url =", url);
			// console.log("geo =", geo);

            var res_adserver = await doRequest(url + geo);

            // console.log("url + geo", url + geo);
            
            // console.log("res_adserver", res_adserver);

			if (res_adserver.statusCode == 200) {
				/* Get data from adserver */ 
				var ret_ad 	= getDataFormAdServer(res_adserver, c_adserver_type);

				if (ret_ad.ad_served == true) {
					r.ad_served 		= ret_ad.ad_served;
					r.ad_type 			= c_ad_type;
					r.adserver_domain 	= c_adserver_domain;
					r.adserver_type 	= c_adserver_type;
					r.creative 			= ret_ad.creative;
					
					var plc_id_tmp = plc_id.split("-");
					
					// var im_tr = "https://"+host+"/"+"v1/ads/log-impression?publisher_id=" + plc_id_tmp[0] + "&placement_id="+plc_id+"&campaign_id=" + r.creative.campaign_id + "&creative_id=" + r.creative.creative_id + "&client_id="+client_id;
					var im_tr = "http://"+host+"/log-impression?publisher_id=" + plc_id_tmp[0] + "&placement_id="+plc_id+"&campaign_id=" + r.creative.campaign_id + "&creative_id=" + r.creative.creative_id + "&client_id="+client_id;
					if (!checkRequired(profile_id)) {
						im_tr += "&profile_id="+profile_id;
					}
					if (geo_cc) {
						im_tr += "&geo_cc="+geo_cc;
					}
					if (geo_region) {
						im_tr += "&geo_region="+geo_region;
					}
					if (geo_city) {
						im_tr += "&geo_city="+geo_city;
					}
					if (geo_province) {
						im_tr += "&geo_province="+geo_province;
					}

					r.creative.impression_trackers = new Array(im_tr);

					// var cl_tr = "https://"+host+"/"+"v1/ads/log-click?publisher_id=" + plc_id_tmp[0] + "&placement_id="+plc_id+"&campaign_id=" + r.creative.campaign_id + "&creative_id=" + r.creative.creative_id + "&client_id="+client_id;
					var cl_tr = "http://"+host+"/log-click?publisher_id=" + plc_id_tmp[0] + "&placement_id="+plc_id+"&campaign_id=" + r.creative.campaign_id + "&creative_id=" + r.creative.creative_id + "&client_id="+client_id;
					if (!checkRequired(profile_id)) {
						cl_tr += "&profile_id="+profile_id;
					}
					if (geo_cc) {
						cl_tr += "&geo_cc="+geo_cc;
					}
					if (geo_region) {
						cl_tr += "&geo_region="+geo_region;
					}
					if (geo_city) {
						cl_tr += "&geo_city="+geo_city;
					}
					if (geo_province) {
						cl_tr += "&geo_province="+geo_province;
					}
					if (!checkRequired(r.creative.click_trackers)) {
						cl_tr += "&click_tracker="+encodeURIComponent(r.creative.click_trackers);
					}

					r.creative.click_trackers = new Array(cl_tr);
					r.creative.creative_url   = r.creative.creative_url + geo;
				} else {
					r.ad_served = false;
				} 

			    if (type == "json") {
			    	res.status(200);

				    if (!checkRequired(callback)) {
				    	res.setHeader('Content-Type', 'application/javascript');
						res.send(
							callback + '(' + JSON.stringify(r) + ')'
						);
				    } else {
				    	res.send(JSON.stringify(r));
						return;
				    }
			    }
			    else {
			    	res.send(errorObj(4002, "Response type is invalid or missing."));
					return;
			    }
			} else {
				res.send(errorObj(4006, "An unknown error has occurred."));
				return;
			}

			res.send(r);
			return;
		} else {
			res.send(errorObj(4005, "Wrong ad server configuration."));
			return;
		}
	} else {
		res.send(errorObj(4005, "Wrong ad server configuration."));
		return;
	}
};

/**
 * 
 * log-impression
 * log-click
 * log-conversion
 * log-audience
 */
async function logImpression(req, res) { 
	/* Default response settings */
    res.setHeader('Content-Type', 'application/json');
	res.status(400);

	var publisher_id, placement_id, campaign_id, creative_id, client_id, output_type, geo_cc, geo_region, geo_city, geo_zip = null, geo_province = null;

	if (req.query.publisher_id) {
		publisher_id = req.query.publisher_id;
	} else {
		res.send(errorObj(5001, "Publisher ID is invalid or missing."));
		return;
	}

	if (req.query.placement_id) {
		placement_id = req.query.placement_id;
	} else {
		res.send(errorObj(5002, "Placement ID is invalid or missing."));
		return;
	}

	if (req.query.campaign_id) {
		campaign_id = req.query.campaign_id;
	} else {
		res.send(errorObj(5003, "Campaign ID is invalid or missing."));
		return;
	}

	if (req.query.creative_id) {
		creative_id = req.query.creative_id;
	} else {
		res.send(errorObj(5004, "Creative ID is invalid or missing."));
		return;
	}
	
	if (req.query.client_id) {
		client_id = req.query.client_id;

		if (!UUID.check(req.query.client_id)) {
			res.send(errorObj(5008, "UUID is invalid or missing."));
			return;
		}
	} else {
		res.send(errorObj(5005, "Client ID is invalid or missing."));
		return;	
	}

	if (typeof req.query.output_type !== "undefined") {
		if (req.query.output_type == "px" || req.query.output_type == "json") {
			output_type = req.query.output_type;
		} else {
			res.send(errorObj(5006, "Output type is invalid (json or px)."));
			return;
		}
	} else {
		output_type = "px";
	}

	if (req.query.impression_tracker) {
		impression_tracker = req.query.impression_tracker;
	}

	if (req.query.geo_cc) {
		geo_cc = req.query.geo_cc;
	} else {
		geo_cc = " ";
	}

	if (req.query.geo_region) {
		geo_region = req.query.geo_region;
	} else {
		geo_region = " ";
	}

	if (req.query.geo_city) {
		geo_city = req.query.geo_city;
	} else {
		geo_city = " ";
	}

	if (req.query.geo_zip) {
		geo_zip = req.query.geo_zip;
	} else {
		geo_zip = " ";
	}

	if (req.query.geo_province) {
		geo_province = req.query.geo_province;
	} else {
		geo_province = " ";
	}

	var ts = Date.now();
	var params = {
		TableName: 'atv_dmp_v1_advertising',
		Item: {
			'client_id': 	{S: client_id},
			'ts':    		{S: ts.toString()},
			'action': 	 	{S: "impression"},
			'publisher_id': {S: publisher_id},
			'placement_id': {S: placement_id},
			'campaign_id': 	{S: campaign_id},
			'creative_id': 	{S: creative_id},
			'geo_cc': 		{S: geo_cc},
			'geo_region': 	{S: geo_region},
			'geo_city': 	{S: geo_city},
			'geo_zip': 	 	{S: geo_zip},
			'geo_province': {S: geo_province}
		}
	};

	/* EPG data */
	if (req.query.epg_type) {
		params.Item['epg_type']          = {S: req.query.epg_type};
	}
	if (req.query.epg_mediaid) {
		params.Item['epg_mediaid']       = {S: req.query.epg_mediaid};
	}
	if (req.query.epg_showid) {
		params.Item['epg_showid']        = {S: req.query.epg_showid};
	}
	if (req.query.epg_title) {
		params.Item['epg_title']         = {S: req.query.epg_title};
	}
	if (req.query.epg_genreid) {
		params.Item['epg_genreid']       = {S: req.query.epg_genreid};
	}
	if (req.query.epg_epgshowname) {
		params.Item['epg_epgshowname']   = {S: req.query.epg_epgshowname};
	}
	if (req.query.epg_seriesid) {
		params.Item['epg_seriesid']      = {S: req.query.epg_seriesid};
	}
	if (req.query.epg_epgseriesname) {
		params.Item['epg_epgseriesname'] = {S: req.query.epg_epgseriesname};
	}
	if (req.query.epg_titleid) {
		params.Item['epg_titleid']       = {S: req.query.epg_titleid};
	}
	if (req.query.epg_epgprogname) {
		params.Item['epg_epgprogname']   = {S: req.query.epg_epgprogname};
	}

	var imp = await putItem(ddb, params);
	if (imp.succeed === true) {
		res.status(200);
		if (output_type === "px")
			return px(res);
		else {
			res.send({success: "ok"});
			return;
		}
	} else {
		console.log("ERROR [logImpression] ", imp.error);
		res.send(errorObj(5007, "An unknown error has occurred."));
		return;
	}
};

async function logClick(req, res) {
	/* Default response settings */
    res.setHeader('Content-Type', 'application/json');
	res.status(400);

	var publisher_id, placement_id, campaign_id, creative_id, client_id, geo_cc, geo_region, geo_city, geo_zip = null, geo_province = null;

	if (req.query.publisher_id) {
		publisher_id = req.query.publisher_id;
	} else {
		res.send(errorObj(6001, "Publisher ID is invalid or missing."));
		return;
	}

	if (req.query.placement_id) {
		placement_id = req.query.placement_id;
	} else {
		res.send(errorObj(6002, "Placement ID is invalid or missing."));
		return;
	}

	if (req.query.campaign_id) {
		campaign_id = req.query.campaign_id;
	} else {
		res.send(errorObj(6003, "Campaign ID is invalid or missing."));
		return;
	}

	if (req.query.creative_id) {
		creative_id = req.query.creative_id;
	} else {
		res.send(errorObj(6004, "Creative ID is invalid or missing."));
		return;
	}
	
	if (req.query.client_id) {
		client_id = req.query.client_id;

		if (!UUID.check(req.query.client_id)) {
			res.send(errorObj(6008, "UUID is invalid or missing."));
			return;
		}
	} else {
		res.send(errorObj(6005, "Client ID is invalid or missing."));
		return;	
	}

	if (typeof req.query.output_type !== "undefined") {
		if (req.query.output_type == "px" || req.query.output_type == "json") {
			output_type = req.query.output_type;
		} else {
			res.send(errorObj(6006, "Output type is invalid (json or px)."));
			return;
		}
	} else {
		output_type = "px";
	}

	if (req.query.geo_cc) {
		geo_cc = req.query.geo_cc;
	} else {
		geo_cc = " ";
	}

	if (req.query.geo_region) {
		geo_region = req.query.geo_region;
	} else {
		geo_region = " ";
	}

	if (req.query.geo_city) {
		geo_city = req.query.geo_city;
	} else {
		geo_city = " ";
	}

	if (req.query.geo_zip) {
		geo_zip = req.query.geo_zip;
	} else {
		geo_zip = " ";
	}

	if (req.query.geo_province) {
		geo_province = req.query.geo_province;
	} else {
		geo_province = " ";
	}

	var ts = Date.now();
	var params = {
		TableName: 'atv_dmp_v1_advertising',
		Item: {
			'client_id': 	{S: client_id},
			'ts':    		{S: ts.toString()},
			'action': 	 	{S: "click"},
			'publisher_id': {S: publisher_id},
			'placement_id': {S: placement_id},
			'campaign_id': 	{S: campaign_id},
			'creative_id': 	{S: creative_id},
			'geo_cc': 		{S: geo_cc},
			'geo_region': 	{S: geo_region},
			'geo_city': 	{S: geo_city},
			'geo_zip': 	 	{S: geo_zip},
			'geo_province': {S: geo_province}
		}
	};

	/* EPG data */
	if (req.query.epg_type) {
		params.Item['epg_type']          = {S: req.query.epg_type};
	}
	if (req.query.epg_mediaid) {
		params.Item['epg_mediaid']       = {S: req.query.epg_mediaid};
	}
	if (req.query.epg_showid) {
		params.Item['epg_showid']        = {S: req.query.epg_showid};
	}
	if (req.query.epg_title) {
		params.Item['epg_title']         = {S: req.query.epg_title};
	}
	if (req.query.epg_genreid) {
		params.Item['epg_genreid']       = {S: req.query.epg_genreid};
	}
	if (req.query.epg_epgshowname) {
		params.Item['epg_epgshowname']   = {S: req.query.epg_epgshowname};
	}
	if (req.query.epg_seriesid) {
		params.Item['epg_seriesid']      = {S: req.query.epg_seriesid};
	}
	if (req.query.epg_epgseriesname) {
		params.Item['epg_epgseriesname'] = {S: req.query.epg_epgseriesname};
	}
	if (req.query.epg_titleid) {
		params.Item['epg_titleid']       = {S: req.query.epg_titleid};
	}
	if (req.query.epg_epgprogname) {
		params.Item['epg_epgprogname']   = {S: req.query.epg_epgprogname};
	}

	var clk = await putItem(ddb, params);
	if (clk.succeed === true) {
		res.status(200);
		if (output_type === "px")
			return px(res);
		else {
			res.send({success: "ok"});
			return;
		}
	} else {
		console.log("ERROR [logClick] ", clk.error);
		res.send(errorObj(6007, "An unknown error has occurred."));
		return;
	}
};

async function logConversion(req, res) {
	/* Default response settings */
	res.setHeader('Content-Type', 'application/json');
	res.status(400);

	var publisher_id, placement_id, campaign_id, creative_id, client_id, geo_cc, geo_region, geo_city, geo_zip = null, geo_province = null;

	if (req.query.publisher_id) {
		publisher_id = req.query.publisher_id;
	} else {
		res.send(errorObj(7001, "Publisher ID is invalid or missing."));
		return;
	}

	if (req.query.placement_id) {
		placement_id = req.query.placement_id;
	} else {
		res.send(errorObj(7002, "Placement ID is invalid or missing."));
		return;
	}

	if (req.query.campaign_id) {
		campaign_id = req.query.campaign_id;
	} else {
		res.send(errorObj(7003, "Campaign ID is invalid or missing."));
		return;
	}

	if (req.query.creative_id) {
		creative_id = req.query.creative_id;
	} else {
		res.send(errorObj(7004, "Creative ID is invalid or missing."));
		return;
	}
	
	if (req.query.client_id) {
		client_id = req.query.client_id;

		if (!UUID.check(req.query.client_id)) {
			res.send(errorObj(7008, "UUID is invalid or missing."));
			return;
		}
	} else {
		res.send(errorObj(7005, "Client ID is invalid or missing."));
		return;	
	}

	if (typeof req.query.output_type !== "undefined") {
		if (req.query.output_type == "px" || req.query.output_type == "json") {
			output_type = req.query.output_type;
		} else {
			res.send(errorObj(7006, "Output type is invalid (json or px)."));
			return;
		}
	} else {
		output_type = "px";
	}

	if (req.query.geo_cc) {
		geo_cc = req.query.geo_cc;
	} else {
		geo_cc = " ";
	}

	if (req.query.geo_region) {
		geo_region = req.query.geo_region;
	} else {
		geo_region = " ";
	}

	if (req.query.geo_city) {
		geo_city = req.query.geo_city;
	} else {
		geo_city = " ";
	}

	if (req.query.geo_zip) {
		geo_zip = req.query.geo_zip;
	} else {
		geo_zip = " ";
	}

	if (req.query.geo_province) {
		geo_province = req.query.geo_province;
	} else {
		geo_province = " ";
	}

	var ts = Date.now();
	var params = {
		TableName: 'atv_dmp_v1_advertising',
		Item: {
			'client_id': 	{S: client_id},
			'ts':    		{S: ts.toString()},
			'action': 	 	{S: "conversion"},
			'publisher_id': {S: publisher_id},
			'placement_id': {S: placement_id},
			'campaign_id': 	{S: campaign_id},
			'creative_id': 	{S: creative_id},
			'geo_cc': 		{S: geo_cc},
			'geo_region': 	{S: geo_region},
			'geo_city': 	{S: geo_city},
			'geo_zip': 	 	{S: geo_zip},
			'geo_province': {S: geo_province}
		}
	};

	/* EPG data */
	if (req.query.epg_type) {
		params.Item['epg_type']          = {S: req.query.epg_type};
	}
	if (req.query.epg_mediaid) {
		params.Item['epg_mediaid']       = {S: req.query.epg_mediaid};
	}
	if (req.query.epg_showid) {
		params.Item['epg_showid']        = {S: req.query.epg_showid};
	}
	if (req.query.epg_title) {
		params.Item['epg_title']         = {S: req.query.epg_title};
	}
	if (req.query.epg_genreid) {
		params.Item['epg_genreid']       = {S: req.query.epg_genreid};
	}
	if (req.query.epg_epgshowname) {
		params.Item['epg_epgshowname']   = {S: req.query.epg_epgshowname};
	}
	if (req.query.epg_seriesid) {
		params.Item['epg_seriesid']      = {S: req.query.epg_seriesid};
	}
	if (req.query.epg_epgseriesname) {
		params.Item['epg_epgseriesname'] = {S: req.query.epg_epgseriesname};
	}
	if (req.query.epg_titleid) {
		params.Item['epg_titleid']       = {S: req.query.epg_titleid};
	}
	if (req.query.epg_epgprogname) {
		params.Item['epg_epgprogname']   = {S: req.query.epg_epgprogname};
	}

	var cnv = await putItem(ddb, params);
	if (cnv.succeed === true) {
		res.status(200);
		if (output_type === "px")
			return px(res);
		else {
			res.send({success: "ok"});
			return;
		}
	} else {
		console.log("ERROR [logConversion] ", cnv.error);
		res.send(errorObj(7007, "An unknown error has occurred."));
		return;
	}
};

async function logAdevent(req, res) {
	/* Default response settings */
	res.setHeader('Content-Type', 'application/json');
	res.status(400);

	var publisher_id, placement_id, campaign_id, creative_id, client_id, ad_action, geo_cc, geo_region, geo_city, geo_zip = null, geo_province = null;

	if (req.query.publisher_id) {
		publisher_id = req.query.publisher_id;
	} else {
		res.send(errorObj(8001, "Publisher ID is invalid or missing."));
		return;
	}

	if (req.query.placement_id) {
		placement_id = req.query.placement_id;
	} else {
		res.send(errorObj(8002, "Placement ID is invalid or missing."));
		return;
	}

	if (req.query.campaign_id) {
		campaign_id = req.query.campaign_id;
	} else {
		res.send(errorObj(8003, "Campaign ID is invalid or missing."));
		return;
	}

	if (req.query.creative_id) {
		creative_id = req.query.creative_id;
	} else {
		res.send(errorObj(8004, "Creative ID is invalid or missing."));
		return;
	}
	
	if (req.query.client_id) {
		client_id = req.query.client_id;

		if (!UUID.check(req.query.client_id)) {
			res.send(errorObj(8008, "UUID is invalid or missing."));
			return;
		}
	} else {
		res.send(errorObj(8005, "Client ID is invalid or missing."));
		return;	
	}

	if (req.query.action) {
		ad_action = req.query.action;
	} else {
		res.send(errorObj(8009, "Ad action is invalid or missing."));
		return;
	}

	if (typeof req.query.output_type !== "undefined") {
		if (req.query.output_type == "px" || req.query.output_type == "json") {
			output_type = req.query.output_type;
		} else {
			res.send(errorObj(8006, "Output type is invalid (json or px)."));
			return;
		}
	} else {
		output_type = "px";
	}

	if (req.query.geo_cc) {
		geo_cc = req.query.geo_cc;
	} else {
		geo_cc = " ";
	}

	if (req.query.geo_region) {
		geo_region = req.query.geo_region;
	} else {
		geo_region = " ";
	}

	if (req.query.geo_city) {
		geo_city = req.query.geo_city;
	} else {
		geo_city = " ";
	}

	if (req.query.geo_zip) {
		geo_zip = req.query.geo_zip;
	} else {
		geo_zip = " ";
	}

	if (req.query.geo_province) {
		geo_province = req.query.geo_province;
	} else {
		geo_province = " ";
	}

	var ts = Date.now();
	var params = {
		TableName: 'atv_dmp_v1_advertising',
		Item: {
			'client_id': 	{S: client_id},
			'ts':    		{S: ts.toString()},
			'action': 	 	{S: ad_action},
			'publisher_id': {S: publisher_id},
			'placement_id': {S: placement_id},
			'campaign_id': 	{S: campaign_id},
			'creative_id': 	{S: creative_id},
			'geo_cc': 		{S: geo_cc},
			'geo_region': 	{S: geo_region},
			'geo_city': 	{S: geo_city},
			'geo_zip': 	 	{S: geo_zip},
			'geo_province': {S: geo_province}
		}
	};

	/* EPG data */
	if (req.query.epg_type) {
		params.Item['epg_type']          = {S: req.query.epg_type};
	}
	if (req.query.epg_mediaid) {
		params.Item['epg_mediaid']       = {S: req.query.epg_mediaid};
	}
	if (req.query.epg_showid) {
		params.Item['epg_showid']        = {S: req.query.epg_showid};
	}
	if (req.query.epg_title) {
		params.Item['epg_title']         = {S: req.query.epg_title};
	}
	if (req.query.epg_genreid) {
		params.Item['epg_genreid']       = {S: req.query.epg_genreid};
	}
	if (req.query.epg_epgshowname) {
		params.Item['epg_epgshowname']   = {S: req.query.epg_epgshowname};
	}
	if (req.query.epg_seriesid) {
		params.Item['epg_seriesid']      = {S: req.query.epg_seriesid};
	}
	if (req.query.epg_epgseriesname) {
		params.Item['epg_epgseriesname'] = {S: req.query.epg_epgseriesname};
	}
	if (req.query.epg_titleid) {
		params.Item['epg_titleid']       = {S: req.query.epg_titleid};
	}
	if (req.query.epg_epgprogname) {
		params.Item['epg_epgprogname']   = {S: req.query.epg_epgprogname};
	}

	var cnv = await putItem(ddb, params);
	if (cnv.succeed === true) {
		res.status(200);
		if (output_type === "px")
			return px(res);
		else {
			res.send({success: "ok"});
			return;
		}
	} else {
		res.send(errorObj(8007, "An unknown error has occurred."));
		return;
	}
};

async function logAudience(req, res) {
	/* Default response settings */
	res.setHeader('Content-Type', 'application/json');
	res.status(400);

	/* init */
	var qs, callback, client, clients;
	var device_id   = "0";
	var profile_id  = "0";
	var client_id 	= "";
	var output_type = "json";
	var d_id_exists = false;
	var p_id_exists = false;
	var consent 	= false;
	var geo_cc = " ", geo_city = " ", geo_region = " ", geo_zip = " ", geo_province = " ", ip_address = " ";

	/* Required */
	if (checkRequired(req.query.publisher_id)) {
		res.send(errorObj(8001, "Publisher ID is invalid or missing."));
		return;
	}

	if (checkRequired(req.query.service_id)) {
		res.send(errorObj(8002, "Service ID is invalid or missing."));
		return;
	}

	if (checkRequired(req.query.platform)) {
		res.send(errorObj(8003, "Platform is invalid or missing."));
		return;
	}

	if (checkRequired(req.query.consent_key)) {
		res.send(errorObj(8004, "Consent key is invalid or missing."));
		return;
	}

	if (checkRequired(req.query.consent_value)) {
		res.send(errorObj(8005, "Consent value is invalid or missing."));
		return;
	} else {
		if (!(req.query.consent_value == "true" || req.query.consent_value == "false")) {
			res.send(errorObj(8006, "An unknown error has occurred."));
			return;
		}
	}

	if (checkRequired(req.query.client_id)) {
		res.send(errorObj(8007, "Client id is invalid or missing."));
		return;
	} else {
		client_id = req.query.client_id;
	}

	/* Optional */
	if (checkOptional(req.query.device_id)) {
		d_id_exists  = true;
		device_id    = req.query.device_id;
	}

	if (checkOptional(req.query.profile_id)) {
		p_id_exists  = true;
		profile_id   = req.query.profile_id;
	}

	if (checkOptional(req.query.geo_cc)) {
		geo_cc       = req.query.geo_cc;
	}

	if (checkOptional(req.query.geo_city)) {
		geo_city     = req.query.geo_city;
	}

	if (checkOptional(req.query.geo_region)) {
		geo_region   = req.query.geo_region;
	}

	if (checkOptional(req.query.geo_zip)) {
		geo_zip      = req.query.geo_zip;
	}

	if (checkOptional(req.query.geo_province)) {
		geo_province = req.query.geo_province;
	}

	if (checkOptional(req.query.ip_address)) {
		ip_address   = req.query.ip_address;
	}

	if (checkOptional(req.query.callback)) {
		output_type  = "jsonp";
		callback     = req.query.callback;
	}

	ip_address = crypto.createHash('md5').update(ip_address).digest("hex");

	/* if (d_id_exists === true) {
		cid_name = cid_name.concat(device_id);
	}

	if (p_id_exists === true) {
		cid_name = cid_name.concat(profile_id);
	}*/
	/* client_id = await getUuid(cid_name); */

	var ua = req.headers['user-agent'];	
	
	console.log("["+req.query.service_id+"] [logAudience] client_id =", client_id);

	client = {
		TableName: 'atv_dmp_v1_clients',
		Key:{
			'client_id' : {S: client_id}
		},
		UpdateExpression: "set brand = :b, device_id = :did, geo_cc = :gcc, geo_city = :gc, geo_region = :gr, geo_zip = :gz, geo_province = :gp, ip_address = :ipa, version = :v",
		ExpressionAttributeValues:{
			':b'  : {S: getBrand(ua)},
			':did': {S: device_id},
			':gcc': {S: geo_cc},
			':gr' : {S: geo_region},
			':gc' : {S: geo_city},
			':gz' : {S: geo_zip},
			':gp' : {S: geo_province},
			':ipa': {S: ip_address},
			':v'  : {S: getVersion(ua)}
		},
	};

	if (p_id_exists) {
		client.ConditionExpression               = "profile_id = :pid";
		client.ExpressionAttributeValues[':pid'] = {S: profile_id};

		/*
		client = {
    		TableName: 'atv_dmp_v1_clients',
		    Key:{
				'client_id' : {S: client_id}
		    },
		    UpdateExpression: "set brand = :b, device_id = :did, geo_cc = :gcc, geo_city = :gc, geo_region = :gr, geo_zip = :gz, geo_province = :gp, ip_address = :ipa, version = :v",
		    ConditionExpression: 'profile_id = :pid',
		    ExpressionAttributeValues:{
		    	':pid': {S: profile_id},
		        ':b'  : {S: getBrand(ua)},
		        ':did': {S: device_id},
		        ':gcc': {S: geo_cc},
		        ':gr' : {S: geo_region},
		        ':gc' : {S: geo_city},
				':gz' : {S: geo_zip},
				':gp' : {S: geo_province},
		        ':ipa': {S: ip_address},
		        ':v'  : {S: getVersion(ua)}
		    },
		};
		*/
	}
	clients = await updateItem(ddb, client);

	/* Audience */
	var ts = Date.now();
	client = {
		TableName: 'atv_dmp_v1_audience',
		Item: {
			'client_id'   : {S: client_id},
			'ts'          : {S: ts.toString()},
			'brand'       : {S: getBrand(ua)},
			'device_id'   : {S: device_id},
			'geo_cc'      : {S: geo_cc},
			'geo_region'  : {S: geo_region},
			'geo_city'    : {S: geo_city},
			'geo_zip'     : {S: geo_zip},
			'geo_province': {S: geo_province},
			'ip_address'  : {S: ip_address},
			'platform'    : {S: req.query.platform},
			'publisher_id': {S: req.query.publisher_id},
			'service_id'  : {S: req.query.service_id},
			'ua'          : {S: ua},
			'version'     : {S: getVersion(ua)}
		}
	};

	var audience = await putItem(ddb, client);

	if (clients.succeed === true && audience.succeed === true) {
		var data = {
			client_id: client_id
		};

		res.status(200);
		if (output_type === "json") {
			res.send(JSON.stringify(data));
		} else {
			res.setHeader('Content-Type', 'application/javascript');
			res.send(
				callback + '(' + JSON.stringify(data) + ')'
			);
		}
		return;
	} else {
		console.log("ERROR [updateClient] ", clients.succeed);
		console.log("ERROR [logAudience] ", audience.error);
		res.send(errorObj(8008, "An unknown error has occurred."));
		return;
	}
};


/**
 * 
 * device-login
 */
async function deviceLogin(req, res) {
	/* Default response settings */
    res.setHeader('Content-Type', 'application/json');
    res.status(400);

    /* init */
	var qs, callback, client, clients, audience, audiences;
	var device_id   = "0";
	var profile_id  = "0";
	var output_type = "json";
	var d_id_exists = false;
    var p_id_exists = false;
    
    /* Required */
    if (checkRequired(req.query.service_id)) {
        res.send(errorObj(1001, "Service ID is invalid or missing."));
		return;
    }
    
    if (checkRequired(req.query.platform)) {
        res.send(errorObj(1002, "Platform is invalid or missing."));
		return;
    }
    
    /* Optional */
	if (checkOptional(req.query.device_id)) {
		d_id_exists = true;
		device_id   = req.query.device_id;
	}

	if (checkOptional(req.query.profile_id)) {
		p_id_exists = true;
		profile_id  = req.query.profile_id;
	}

	if (checkOptional(req.query.callback)) {
		output_type = "jsonp";
		callback    = req.query.callback;
    }
    
    var ip         = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
		ip         = ip.replace(/^.*:/, '');
    var ua         = req.headers['user-agent']; 								
    var ip_address = crypto.createHash('md5').update(ip).digest("hex");
    var cid_name   = ip.concat(ua);    
    
	if (d_id_exists === true) cid_name = cid_name.concat(device_id);
	if (p_id_exists === true) cid_name = cid_name.concat(profile_id);

	var client_id = await getUuid(cid_name);

	/* Get location data */
	var location;
	try {
		location = await getGeo(ip);    
		location = JSON.parse(location);
	} catch (e) {
		res.send(errorObj(1003, "An unknown error has occurred."));
		return;
    }

    var params = {
	    TableName : "atv_dmp_v1_clients",
	    ExpressionAttributeValues: {
		    ':c_id': {S: client_id}
		},
		KeyConditionExpression: 'client_id = :c_id'
    };	
    
	var client_login = await getItem(ddb, params);

	if (client_login.data.length > 0) {
        client = {
            TableName: 'atv_dmp_v1_clients',
            Key:{
                'client_id': {S: client_id}
            },
            UpdateExpression: "set brand = :b, device_id = :did, geo_cc = :gcc, geo_city = :gc, geo_region = :gr, geo_zip = :gz, geo_province = :gp, ip_address = :ipa, version = :v",
            ExpressionAttributeValues:{
                ':b'  : {S: getBrand(ua)},
                ':did': {S: device_id},
                ':gcc': {S: location.geo_cc},
                ':gr' : {S: location.geo_region},
                ':gc' : {S: location.geo_city},
				':gz' : {S: location.geo_zip},
				':gp' : {S: location.geo_province},
                ':ipa': {S: ip_address},
                ':v'  : {S: getVersion(ua)}
            },
        };

        if (p_id_exists) {
            client.ConditionExpression = 'profile_id = :pid';
            client.ExpressionAttributeValues[':pid'] = {S: profile_id};
        }

		clients = await updateItem(ddb, client);
	} else {
        client = {
            TableName: 'atv_dmp_v1_clients',
            Item: {
                'client_id'   : {S: client_id},
                'brand'       : {S: getBrand(ua)},
                'device_id'   : {S: device_id},
                'geo_cc'      : {S: location.geo_cc},
                'geo_region'  : {S: location.geo_region},
                'geo_city'    : {S: location.geo_city},
				'geo_zip'     : {S: location.geo_zip},
				'geo_province': {S: location.geo_province},
                'ip_address'  : {S: ip_address},
                'version'     : {S: getVersion(ua)}
            }
        };

        if (p_id_exists) {
            client.Item['profile_id'] = {S: profile_id};
        }

		clients = await putItem(ddb, client);
    }

	/* Audience */
	var ts = Date.now();
	audience = {
		TableName: 'atv_dmp_v1_audience',
		Item: {
			'client_id'   : {S: client_id},
			'ts'          : {S: ts.toString()},
			'brand'       : {S: getBrand(ua)},
			'device_id'   : {S: device_id},
			'geo_cc'      : {S: location.geo_cc},
			'geo_region'  : {S: location.geo_region},
			'geo_city'    : {S: location.geo_city},
			'geo_zip'     : {S: location.geo_zip},
			'geo_province': {S: location.geo_province},
			'ip_address'  : {S: ip_address},
			'platform'    : {S: req.query.platform},
			'service_id'  : {S: req.query.service_id},
			'ua'          : {S: ua},
            'version'     : {S: getVersion(ua)}
		}
    };

    try {
        audiences = await putItem(ddb, audience);
    } catch(e) {
        console.log(e.message);
    }

	if (clients.succeed === true && audiences.succeed === true) {
		var data = {
			client_id:    client_id,
			geo_cc:       location.geo_cc,
			geo_region:   location.geo_region,
			geo_city:     location.geo_city,
			geo_zip:      location.geo_zip,
			geo_province: location.geo_province
        };
    
        res.status(200);
		if (output_type === "json") {
            res.send(JSON.stringify(data));
            return;
		} else {            
            return jsonp(res, callback, data);
		}
	} else {
        res.send(errorObj(1003, "An unknown error has occurred."));
		return;
    }
};




/**
 * 
 * Functions
 */
async function getUuid(name) {
	/* return new promise */ 
	return new Promise(function(resolve, reject) {
        /* do async job */
        UUID.v5({
	    	namespace: UUID.namespace.cstm, /* url */
	    	name:      name
		}, function (err, result) {
	      	if (err === null) {
	        	resolve(result);
	      	} else {
	        	reject(result);
	      	}
	    });
    })  
};

async function getItem(ddb, item) {
	/* return new promise */ 
	return new Promise(function(resolve, reject) {
        try {
        	ddb.query(item, function(err, data) {
			    if (err) {
					console.log(err); reject({succeed: false});
			    } else {
			    	resolve({data: data.Items});
			    }
			});
        } catch (e) {
			reject({succeed: false});
		}
    })
};

async function putItem(ddb, item) {
	/* return new promise */ 
	return new Promise(function(resolve, reject) {
        try {
        	ddb.putItem(item, function(err, data) {
			    if (err) {
					// console.log(err);
					reject({succeed: false, error: err});
				}
			    else {
					resolve({succeed: true});
				}
			});
        } catch (e) {
			// console.log(e);
			reject({succeed: false, error: e});
		}
    })
};

async function updateItem(ddb, item) {
	/* return new promise */ 
	return new Promise(function(resolve, reject) {
        try {
        	ddb.updateItem(item, function(err, data) {
			    if (err) {
					console.log(err); reject({succeed: false});
				} else
			    	resolve({succeed: true});
			});
        } catch (e) {
			reject({succeed: false});
		}
    })
};

async function doRequest(url) {
    /* return new promise */ 
    return new Promise(function(resolve, reject) {
        /* do async job */
        request(url, function(error, response, body) { 
        	if (!error && response.statusCode == 200) {
	        	resolve(response);
	      	} else {
	        	reject(error);
	      	}
        })
    });
};

async function getGeo(ip_address) {
	// var url = "http://3.121.209.57/geo/index.php?ip="+ip_address;
	var url = "http://microservices.castoola.tv/geo/index.php?ip="+ip_address; 
	
    /* return new promise */ 
    return new Promise(function(resolve, reject) {
        /* do async job */
        request(url, function(error, response, body) { 
        	if (!error && response.statusCode == 200) {
	        	resolve(response.body);
			  } else {
				console.log("[getGeo] error = ", error);
	        	reject(error);
	      	}
        })
    });
};

function getVersion(ua) {
	ua = ua.toLowerCase();
	var version = "unknown";

	if (ua.indexOf("hbbtv/1.1.1") !== -1) {
        version = "HbbTV/1.1";
    } else if (ua.indexOf("hbbtv/1.2.1") !== -1) {
        version = "HbbTV/1.5";
    } else if (ua.indexOf("hbbtv/1.3.1") !== -1) {
        version = "HbbTV/2.0";
    } else if (ua.indexOf("hbbtv/1.4.1") !== -1) {
        version = "HbbTV/2.0.1";
    } else if (ua.indexOf("hbbtv/1.5.1") !== -1) {
        version = "HbbTV/2.0.2";
    } 

    return version;
};

function getBrand(ua) {
	ua = ua.toLowerCase();
	var brand = "unknown";
	var brands = ["samsung", "philips", "sony", "lge", "panasonic", "toshiba", "vestel", "jvc", "firetv", "hisense", "sharp", "mstar", "tcl", "tpvision", "tesla", "grundig", "medion", "hitachi", "hyundai", "manta", "orion", "nec"];

	brands.forEach(function(b) {
		if (ua.indexOf(b) !== -1) {
	        brand = b;
	        return brand;
	    }
	});

    return brand;
};

function getDataFormAdServer(data, adserver_type) {
	var ret = {};
	ret.creative = {};
	if (adserver_type == "atv-revive") {
		var json = JSON.parse(data.body);
		if (json.ad_served == true) {
			ret.ad_served = true;
			ret.creative.campaign_id 			= json.campaign_id;
			ret.creative.price  				= parseFloat(json.price);
			ret.creative.creative_id   			= json.creative_id;
			ret.creative.creative_url    		= json.imageUrl;
			ret.creative.width    				= json.width;
			ret.creative.height    				= json.height;
			ret.creative.impression_trackers	= new Array(json.impressionUrl);
			ret.creative.click_trackers			= new Array(json.clickUrl);
			ret.creative.conversion_trackers	= new Array(json.conversionUrl);
			// ret.creative.click_url			= json.clickUrl;
			// ret.creative.impression_url		= json.impressionUrl;
		} else {
			ret.creative.ad_served = false;
		}
	}

	return ret;
};

function checkRequired(param) {
	if (typeof param === "undefined" || param === null || param === "") {
		return true;
	}
	return false;
};

function checkOptional(param) {
	if (typeof param !== "undefined" && param !== null && param !== "") {
		return true;
	}
	return false;
};

function errorObj(error_code, message) {
	return {
    	error_code: error_code,
      	message:    message
	};
};

function jsonp(res, callback, data) {
	res.setHeader('Content-Type', 'application/javascript');
	res.send(
		callback + '(' + JSON.stringify(data) + ')'
	);
	return;
};

function px(res) {
	res.status(200);
	res.set('Access-Control-Allow-Origin', '*');
	res.set('Content-Type', 'image/gif');
	res.set('isBase64Encoded', true);
	res.send(imageHex);
	return;
};
